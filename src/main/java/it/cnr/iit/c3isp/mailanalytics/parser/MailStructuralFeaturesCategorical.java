package it.cnr.iit.c3isp.mailanalytics.parser;

/**
 * This class represents the structural features of a mail
 * 
 * @author antonio
 *
 */
public class MailStructuralFeaturesCategorical {
  private int ipBasedLinks;
  private int mismatchingLinks;
  private int numberOfLinks;
  private int numberOfDomainsInLinks;
  private int dotsPerlink;
  private int linksWithAt;
  private int linksWithHex;
  private int nonAsciiLinks;
  private int isHtml;
  private int mailSize;
  private int language;
  private int attachmentNumber;
  private int avgAttachmentSize;
  private int attachmentType;
  private int wordsInSubject;
  private int subjectSize;
  private int imagesNumber;
  private int nonAsciiCharsInSubject;
  private int isReOrFwdInSubject;
  private int recipientNumber;

  public int getIpBasedLinks() {
    return ipBasedLinks;
  }

  public void setIpBasedLinks(int ipBasedLinks) {
    this.ipBasedLinks = ipBasedLinks;
  }

  public int getMismatchingLinks() {
    return mismatchingLinks;
  }

  public void setMismatchingLinks(int mismatchingLinks) {
    this.mismatchingLinks = mismatchingLinks;
  }

  public int getNumberOfLinks() {
    return numberOfLinks;
  }

  public void setNumberOfLinks(int numberOfLinks) {
    this.numberOfLinks = numberOfLinks;
  }

  public int getNumberOfDomainsInLinks() {
    return numberOfDomainsInLinks;
  }

  public void setNumberOfDomainsInLinks(int numberOfDomainsInLinks) {
    this.numberOfDomainsInLinks = numberOfDomainsInLinks;
  }

  public int getDotsPerlink() {
    return dotsPerlink;
  }

  public void setDotsPerlink(int dotsPerlink) {
    this.dotsPerlink = dotsPerlink;
  }

  public int getLinksWithAt() {
    return linksWithAt;
  }

  public void setLinksWithAt(int linksWithAt) {
    this.linksWithAt = linksWithAt;
  }

  public int getLinksWithHex() {
    return linksWithHex;
  }

  public void setLinksWithHex(int linksWithHex) {
    this.linksWithHex = linksWithHex;
  }

  public int getNonAsciiLinks() {
    return nonAsciiLinks;
  }

  public void setNonAsciiLinks(int nonAsciiLinks) {
    this.nonAsciiLinks = nonAsciiLinks;
  }

  public int getIsHtml() {
    return isHtml;
  }

  public void setIsHtml(int isHtml) {
    this.isHtml = isHtml;
  }

  public int getMailSize() {
    return mailSize;
  }

  public void setMailSize(int mailSize) {
    this.mailSize = mailSize;
  }

  public int getLanguage() {
    return language;
  }

  public void setLanguage(int language) {
    this.language = language;
  }

  public int getAttachmentNumber() {
    return attachmentNumber;
  }

  public void setAttachmentNumber(int attachmentNumber) {
    this.attachmentNumber = attachmentNumber;
  }

  public int getAvgAttachmentSize() {
    return avgAttachmentSize;
  }

  public void setAvgAttachmentSize(int avgAttachmentSize) {
    this.avgAttachmentSize = avgAttachmentSize;
  }

  public int getAttachmentType() {
    return attachmentType;
  }

  public void setAttachmentType(int attachmentType) {
    this.attachmentType = attachmentType;
  }

  public int getWordsInSubject() {
    return wordsInSubject;
  }

  public void setWordsInSubject(int wordsInSubject) {
    this.wordsInSubject = wordsInSubject;
  }

  public int getSubjectSize() {
    return subjectSize;
  }

  public void setSubjectSize(int subjectSize) {
    this.subjectSize = subjectSize;
  }

  public int getImagesNumber() {
    return imagesNumber;
  }

  public void setImagesNumber(int imagesNumber) {
    this.imagesNumber = imagesNumber;
  }

  public int getNonAsciiCharsInSubject() {
    return nonAsciiCharsInSubject;
  }

  public void setNonAsciiCharsInSubject(int nonAsciiCharsInSubject) {
    this.nonAsciiCharsInSubject = nonAsciiCharsInSubject;
  }

  public int getIsReOrFwdInSubject() {
    return isReOrFwdInSubject;
  }

  public void setIsReOrFwdInSubject(int isReOrFwdInSubject) {
    this.isReOrFwdInSubject = isReOrFwdInSubject;
  }

  public int getRecipientNumber() {
    return recipientNumber;
  }

  public void setRecipientNumber(int recipientNumber) {
    this.recipientNumber = recipientNumber;
  }

  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(ipBasedLinks);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.mismatchingLinks);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.numberOfLinks);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.numberOfDomainsInLinks);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.dotsPerlink);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.linksWithAt);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.linksWithHex);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.nonAsciiLinks);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.isHtml);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.mailSize);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.language);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.attachmentNumber);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.avgAttachmentSize);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.attachmentType);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.wordsInSubject);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.subjectSize);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.imagesNumber);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.nonAsciiCharsInSubject);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.isReOrFwdInSubject);
    appendConcatenation(stringBuilder);
    stringBuilder.append(this.recipientNumber);
    return stringBuilder.toString();
  }

  private StringBuilder appendConcatenation(StringBuilder stringBuilder) {
    stringBuilder.append(",");
    return stringBuilder;
  }
}
