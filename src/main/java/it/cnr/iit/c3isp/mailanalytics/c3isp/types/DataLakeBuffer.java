package it.cnr.iit.c3isp.mailanalytics.c3isp.types;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Return of the ISI API when calling the prepare data, in reality these data
 * are returned by the buffer manager
 * 
 * @author antonio
 *
 */
public class DataLakeBuffer {
  @JsonProperty("data")
  private DpoInfo[] data;
  @JsonProperty("datalake")
  private DataLake datalake;

  public DataLakeBuffer() {

  }

  public DpoInfo[] getData() {
    return data;
  }

  public void setData(DpoInfo[] data) {
    this.data = data;
  }

  public DataLake getDatalake() {
    return datalake;
  }

  public void setDatalake(DataLake datalake) {
    this.datalake = datalake;
  }

  public String getDpoID(String fileName) {
    if (fileName == null) {
      return "empty";
    }
    for (DpoInfo dpoInfo : data) {
      if (dpoInfo.getFile() != null && dpoInfo.getFile().contains(fileName)) {
        return dpoInfo.getDPO_id();
      }
    }
    return "empty";
  }

}
