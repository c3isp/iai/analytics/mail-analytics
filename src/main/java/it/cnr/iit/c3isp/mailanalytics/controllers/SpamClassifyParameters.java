package it.cnr.iit.c3isp.mailanalytics.controllers;

import com.fasterxml.jackson.annotation.JsonProperty;

import it.cnr.iit.c3isp.mailanalytics.c3isp.types.DataLakeBuffer;

public class SpamClassifyParameters {

	@JsonProperty("dataLakeBuffer")
	private DataLakeBuffer dataLakeBuffer;
	@JsonProperty("featuresOnly")
	private boolean isFeaturesOnly = false;

	public SpamClassifyParameters() {
	}

	public DataLakeBuffer getDataLakeBuffer() {
		return dataLakeBuffer;
	}

	public void setDataLakeBuffer(DataLakeBuffer dataLakeBuffer) {
		this.dataLakeBuffer = dataLakeBuffer;
	}

	public boolean isFeaturesOnly() {
		return isFeaturesOnly;
	}

	public void setFeaturesOnly(boolean isFeaturesOnly) {
		this.isFeaturesOnly = isFeaturesOnly;
	}

}
