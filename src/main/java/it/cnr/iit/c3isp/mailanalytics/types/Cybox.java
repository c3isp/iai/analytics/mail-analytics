
package it.cnr.iit.c3isp.mailanalytics.types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "spec_version",
    "objects"
})
public class Cybox {

    @JsonProperty("spec_version")
    private String specVersion;
    @JsonProperty("objects")
    private List<Object_> objects = null;
    @JsonIgnore
    private Map<String, java.lang.Object> additionalProperties = new HashMap<String, java.lang.Object>();

    @JsonProperty("spec_version")
    public String getSpecVersion() {
        return specVersion;
    }

    @JsonProperty("spec_version")
    public void setSpecVersion(String specVersion) {
        this.specVersion = specVersion;
    }

    @JsonProperty("objects")
    public List<Object_> getObjects() {
        return objects;
    }

    @JsonProperty("objects")
    public void setObjects(List<Object_> objects) {
        this.objects = objects;
    }

    @JsonAnyGetter
    public Map<String, java.lang.Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, java.lang.Object value) {
        this.additionalProperties.put(name, value);
    }

}
