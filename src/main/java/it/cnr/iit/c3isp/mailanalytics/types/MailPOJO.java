
package it.cnr.iit.c3isp.mailanalytics.types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "spec_version",
    "type",
    "id",
    "objects"
})
public class MailPOJO {

    @JsonProperty("spec_version")
    private String specVersion;
    @JsonProperty("type")
    private String type;
    @JsonProperty("id")
    private String id;
    @JsonProperty("objects")
    private List<it.cnr.iit.c3isp.mailanalytics.types.Object> objects = null;
    @JsonIgnore
    private Map<String, java.lang.Object> additionalProperties = new HashMap<String, java.lang.Object>();

    @JsonProperty("spec_version")
    public String getSpecVersion() {
        return specVersion;
    }

    @JsonProperty("spec_version")
    public void setSpecVersion(String specVersion) {
        this.specVersion = specVersion;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("objects")
    public List<it.cnr.iit.c3isp.mailanalytics.types.Object> getObjects() {
        return objects;
    }

    @JsonProperty("objects")
    public void setObjects(List<it.cnr.iit.c3isp.mailanalytics.types.Object> objects) {
        this.objects = objects;
    }

    @JsonAnyGetter
    public Map<String, java.lang.Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, java.lang.Object value) {
        this.additionalProperties.put(name, value);
    }

}
