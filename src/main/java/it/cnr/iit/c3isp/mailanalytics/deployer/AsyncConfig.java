package it.cnr.iit.c3isp.mailanalytics.deployer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@EnableAsync
public class AsyncConfig implements AsyncConfigurer {

	@Override
	public ExecutorService getAsyncExecutor() {
		ExecutorService executor = Executors.newFixedThreadPool(10);
		return executor;
	}

}
