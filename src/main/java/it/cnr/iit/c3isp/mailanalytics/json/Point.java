package it.cnr.iit.c3isp.mailanalytics.json;

/**
 * This class represents a point in a map.
 * 
 * @author antonio
 *
 */
final public class Point {
  private double longitude;
  private double latitude;

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  /**
   * FIXME
   */
  public String toString() {
    return longitude + "-" + latitude;
  }
}
