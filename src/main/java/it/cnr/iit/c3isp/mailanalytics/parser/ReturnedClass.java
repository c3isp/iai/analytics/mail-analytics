package it.cnr.iit.c3isp.mailanalytics.parser;

public class ReturnedClass {
  private String mailArray;
  private String globe;

  public ReturnedClass(String mailArray, String globe) {
    this.globe = globe;
    this.mailArray = mailArray;
  }
}
