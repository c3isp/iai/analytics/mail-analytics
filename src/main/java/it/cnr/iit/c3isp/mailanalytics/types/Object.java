
package it.cnr.iit.c3isp.mailanalytics.types;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "id",
    "created",
    "modified",
    "first_observed",
    "last_observed",
    "cybox"
})
public class Object {

    @JsonProperty("type")
    private String type;
    @JsonProperty("id")
    private String id;
    @JsonProperty("created")
    private String created;
    @JsonProperty("modified")
    private String modified;
    @JsonProperty("first_observed")
    private String firstObserved;
    @JsonProperty("last_observed")
    private String lastObserved;
    @JsonProperty("cybox")
    private Cybox cybox;
    @JsonIgnore
    private Map<String, java.lang.Object> additionalProperties = new HashMap<String, java.lang.Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    @JsonProperty("modified")
    public String getModified() {
        return modified;
    }

    @JsonProperty("modified")
    public void setModified(String modified) {
        this.modified = modified;
    }

    @JsonProperty("first_observed")
    public String getFirstObserved() {
        return firstObserved;
    }

    @JsonProperty("first_observed")
    public void setFirstObserved(String firstObserved) {
        this.firstObserved = firstObserved;
    }

    @JsonProperty("last_observed")
    public String getLastObserved() {
        return lastObserved;
    }

    @JsonProperty("last_observed")
    public void setLastObserved(String lastObserved) {
        this.lastObserved = lastObserved;
    }

    @JsonProperty("cybox")
    public Cybox getCybox() {
        return cybox;
    }

    @JsonProperty("cybox")
    public void setCybox(Cybox cybox) {
        this.cybox = cybox;
    }

    @JsonAnyGetter
    public Map<String, java.lang.Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, java.lang.Object value) {
        this.additionalProperties.put(name, value);
    }

}
