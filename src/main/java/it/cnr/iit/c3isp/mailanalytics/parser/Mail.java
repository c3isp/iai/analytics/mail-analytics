package it.cnr.iit.c3isp.mailanalytics.parser;

import java.util.ArrayList;
import java.util.Date;

import iit.cnr.it.classificationengine.basic.Element;
import it.cnr.iit.c3isp.mailanalytics.json.Point;

public class Mail {

	private MailStructuralFeaturesCategorical features;

	int id;
	private int language;
	private int size;
	private String domain;
	private int domainType;
	private String subject;
	private int layout;
	private int attachmentType;
	private int messageType;
	protected String mailText;
	private String charset;
	protected String recipient;
	protected String sender;
	private String contentType; // used to compute messageType
	private String receivingDate;
	private String user_name;

	private ArrayList<String> transversedMailServers;
	private Point senderPosition;
	private Point recipientPosition;

	private String senderIp;
	private String recipientIp;

	private ArrayList<String> hrefs;
	protected ArrayList<String> texts; // used to find mismatching links

	private String fileName;

	public Mail(int id) {
		this.id = id;
	}

	public Mail(Mail mail) {
		id = mail.id;
		language = mail.language;
		size = mail.size;
		domain = mail.domain;
		domainType = mail.domainType;
		subject = mail.subject;
		layout = mail.layout;
		attachmentType = mail.attachmentType;
		messageType = mail.messageType;
		mailText = mail.mailText;
		charset = mail.charset;
		recipient = mail.recipient;
		sender = mail.sender;
		contentType = mail.contentType;
		receivingDate = mail.receivingDate;
		user_name = mail.user_name;
		transversedMailServers = new ArrayList<String>(mail.transversedMailServers);
		senderPosition = mail.senderPosition;
		recipientPosition = mail.recipientPosition;
		senderIp = mail.senderIp;
		recipientIp = mail.recipientIp;
		hrefs = new ArrayList<>(mail.hrefs);
		texts = new ArrayList<>(mail.texts);
	}

	public int getId() {
		return id;
	}

	public int getLanguage() {
		return language;
	}

	public void setuser(String user) {
		user_name = user;
	}

	public String getUser() {
		return user_name;
	}

	public void setSenderIp(String senderIp) {
		this.senderIp = senderIp;
	}

	public String getSenderIp() {
		return senderIp;
	}

	public void setRecipientIp(String recipientIp) {
		this.recipientIp = recipientIp;
	}

	public String getRecipientIp() {
		return recipientIp;
	}

	public void setTraversedMailServers(ArrayList<String> transversedMailServers) {
		this.transversedMailServers = transversedMailServers;
	}

	public ArrayList<String> getTraversedMailServers() {
		return transversedMailServers;
	}

	public void setLanguage(int language) {
		this.language = language;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getDomainType() {
		return domainType;
	}

	public void setDomainType(int domainType) {
		this.domainType = domainType;
	}

	public int getAttachmentType() {
		return attachmentType;
	}

	public void setAttachmentType(int attachmentType) {
		this.attachmentType = attachmentType;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getLayout() {
		return layout;
	}

	public void setLayout(int layout) {
		this.layout = layout;
	}

	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	public String getMailText() {
		return mailText;
	}

	public void setMailText(String mailText) {
		this.mailText = mailText;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getContentType() {
		return (contentType != null) ? contentType : "";
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getReceivingDate() {
		return receivingDate;
	}

	public void setReceivingDate(String receivingDate) {
		this.receivingDate = receivingDate;
	}

	public void setReceivingDate(Date receivingDate) {
		this.receivingDate = receivingDate.toString();
	}

	public ArrayList<String> getTexts() {
		return texts;
	}

	public void setTexts(ArrayList<String> texts) {
		this.texts = texts;
	}

	public ArrayList<String> getHrefs() {
		return hrefs;
	}

	public void setHrefs(ArrayList<String> hrefs) {
		this.hrefs = hrefs;
	}

	public Point getSenderPosition() {
		return senderPosition;
	}

	public void setSenderPosition(Point senderPosition) {
		this.senderPosition = senderPosition;
	}

	public Point getRecipientPosition() {
		return recipientPosition;
	}

	public void setRecipientPosition(Point recipientPosition) {
		this.recipientPosition = recipientPosition;
	}

	public static final Element<Integer> getMailFeaturesWithID(Mail mail) {

		String features = mail.getId() + "," + mail.getFeatures().toString();
		Element<Integer> element = Element.buildClustererElement(features, ",", Integer.class);
		return element;

	}

	public MailStructuralFeaturesCategorical getFeatures() {
		return features;
	}

	public void setFeatures(MailStructuralFeaturesCategorical features) {
		this.features = features;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	@Override
	public String toString() {
		return getMailFeaturesWithID(this).toString();
	}

}
