package it.cnr.iit.c3isp.mailanalytics.c3isp.types;

import java.util.ArrayList;

public class Paths {
  private ArrayList<String> paths = new ArrayList<>();

  public void addPath(String path) {
    if (path != null && path.length() > 0) {
      paths.add(path);
    }
  }

  public ArrayList<String> getPaths() {
    return paths;
  }

  public void setPaths(ArrayList<String> paths) {
    this.paths = paths;
  }

}
