package it.cnr.iit.c3isp.mailanalytics.json.chart;

public class StackedValue {
  String x = "";
  int y = 0;
  String y0 = "";
  int y1 = 0;

  public StackedValue() {

  }

  public void setX(String x) {
    this.x = x;
    this.y0 = x;
  }

  public void setY(int y) {
    this.y = y;
    this.y1 = y;
  }

  public String getX() {
    return this.x;
  }

  public int getY() {
    return this.y;
  }

  public String getY0() {
    return this.y0;
  }

  public int getY1() {
    return this.y1;
  }
}
