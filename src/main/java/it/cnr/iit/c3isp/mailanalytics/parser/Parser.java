package it.cnr.iit.c3isp.mailanalytics.parser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
//import java.util.Scanner;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import it.cnr.iit.c3isp.mailanalytics.json.Point;

import java.util.logging.Logger;

public class Parser {

	private static final Logger log = Logger.getLogger(Parser.class.getName());

	private BufferedInputStream filePath;
	private String content;
	private Mail mail;

	// private static String dbfile =
	// "/home/antonio/projects/gitlab/IAI-API/src/geoip_db/GeoLiteCity.dat";

	public Mail getMail() {
		return this.mail;
	}

	public Parser(InputStream path, long size) {
		filePath = new BufferedInputStream(path);
		mail.setSize((int) size);
		content = null;
	}

	public Parser(InputStream path, Mail mail) throws Exception {
		filePath = new BufferedInputStream(path);
		this.mail = mail;
		content = null;
	}

	public Parser(String mailContent, Mail mail) throws Exception {
		filePath = null;
		this.mail = mail;
		content = mailContent;
	}

	public void parse() throws Exception {
		if (content == null)
			content = readMail();
		parseHeader(mail, content);
		parseText(mail, content);
	}

	private String readMail() {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(filePath))) {

			StringBuilder stringBuilder = new StringBuilder();
			String tmp;
			while ((tmp = br.readLine()) != null) {
				stringBuilder.append(tmp + "\n");
			}
			content = stringBuilder.toString();
			br.close();
			return content;
		} catch (IOException e1) {
			e1.printStackTrace();
			return null;
		}
	}

	private void parseHeader(Mail mail, String content) {
		mail.setSize(content.length());
		mail.setRecipient(getFromMail(content, "To"));
		mail.setSender(getFromMail(content, "From"));
		mail.setContentType(getFromMail(content, "Content-Type"));
		mail.setSubject(getFromMail(content, "Subject"));
		mail.setReceivingDate(parseDate());
		mail.setTraversedMailServers(getIPFromMail(content, "Received"));
		setSenderAndRecipient(mail);
		mail.setContentType(lookForMessageType(content));
	}

	private ArrayList<String> getIPFromMail(String content, String element) {
		String tempLine = "";
		ArrayList<String> ipList = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new ByteArrayInputStream(content.getBytes())))) {
			boolean find = false;
			StringTokenizer st;
			while ((tempLine = br.readLine()) != null) {
				if (!tempLine.contains("Received:") & !find)
					continue;
				else {
					find = true;
					if (!tempLine.contains("Received:") & tempLine.split(" ")[0].contains(":"))
						break;
					else {
						String ip = getIp(tempLine);
						if (ip != null && !isPrivateIP(ip)) {
							ipList.add(ip);
						}
					}
				}

			}
		} catch (NoSuchElementException ne) {
			ne.printStackTrace();
			return ipList;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ipList;
	}

	private String getIp(String s) {
		java.util.regex.Matcher m = java.util.regex.Pattern.compile("(?<!\\d|\\d\\.)"
				+ "(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
				+ "(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." + "(?:[01]?\\d\\d?|2[0-4]\\d|25[0-5])" + "(?!\\d|\\.\\d)")
				.matcher(s);
		return m.find() ? m.group() : null;
	}

	private void parseText(Mail mail, String content) {
		mail.setMailText(getMailText(content));
		mail.setHrefs(getAllHref(content));
		mail.setTexts(getAllTextInHref(content));
		mail.setFeatures(extractStructuralFeatures(content, mail));
	}

	private void setSenderAndRecipient(Mail mail) {
		if ((mail.getTraversedMailServers().size() <= 1) || (mail.getTraversedMailServers().get(0)
				.equals(mail.getTraversedMailServers().get(mail.getTraversedMailServers().size() - 1)))) {
		} else {
			mail.setSenderIp(mail.getTraversedMailServers().get(mail.getTraversedMailServers().size() - 1));
			mail.setRecipientIp(mail.getTraversedMailServers().get(0));
			Point src_location = getPositionFromIp(mail.getSenderIp());
			Point dst_location = getPositionFromIp(mail.getRecipientIp());
			mail.setRecipientPosition(dst_location);
			mail.setSenderPosition(src_location);
		}
	}

	private String parseDate() {
		String tempDate = this.getLineFromMail("Date");
		try {
			mail.setReceivingDate(new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH)
					.parse(tempDate.substring(6, tempDate.length())));
		} catch (ParseException e) {
			System.out.println("ERROR: Invalid date format");
		}
		return tempDate;
	}

	private MailStructuralFeaturesCategorical extractStructuralFeatures(String content, Mail mail) {
		MailStructuralFeaturesCategorical features = new MailStructuralFeaturesCategorical();
		features.setIpBasedLinks(categorizeIpAttribute(getNumberOfIpBasedLinks(mail)));
		features.setMismatchingLinks(categorizeMismatchingLinks(getNumberOfMismatchingLinks(mail)));
		features.setNumberOfLinks(categNumOfLinks(getNumberOfLinks(mail)));
		features.setNumberOfDomainsInLinks(categNumOfDomains(getNumberOfDomainsInLinks(mail)));
		features.setDotsPerlink(categAvgDots(getAvgDotsPerLink(mail)));
		features.setLinksWithAt(categLinksWithAt(getNumberOfLinksWithAt(mail)));
		features.setLinksWithHex(categLinksWithHex(getNumberOfLinksWithHex(mail)));
		features.setNonAsciiLinks(categNonAsciiLinks(getNumberOfNonAsciiLinks(mail)));
		features.setIsHtml(checkIfHtml(mail));
		features.setMailSize(categMailSize(content.length()));
		// FIXME find a way to extract language from mail
		features.setLanguage(GlobalVars.ENGLISH_LANG);
		features.setAttachmentNumber(categNumAttach(getAttachmentNumber(mail)));
		if (getAttachmentNumber(mail) == 0) {
			features.setAvgAttachmentSize(-1);
			features.setAttachmentType(GlobalVars.NONE);
		} else {
			features.setAvgAttachmentSize(categAttSize(retrieveAttSize(mail)));
			if (getAttachmentNumber(mail) == 1) {
				features.setAttachmentType(categAttachmentType(getAttachmentType(mail)[0]));
			} else {
				features.setAttachmentType(GlobalVars.OTHER);
			}
		}
		features.setWordsInSubject(categNumWordSubj(Utilities.wordCount(mail.getSubject())));
		features.setSubjectSize(categNumCharsInSubj(mail.getSubject().length()));
		features.setImagesNumber(categNumImages(Utilities.countOccurencies("img src", mail)));
		features.setNonAsciiCharsInSubject(
				categNumNonAsciiCharInSubj(Utilities.countNonAsciiCharacters(mail.getSubject())));
		features.setIsReOrFwdInSubject(findReOrFwdInSubject(mail.getSubject()));
		features.setRecipientNumber(categCountRecipients(Utilities.countEmailsInLine(mail.getRecipient())));
		return features;
	}

	private int getNumberOfIpBasedLinks(Mail mail) {
		ArrayList<String> tempContainer = mail.getHrefs();
		int amount = 0;
		for (int i = 0; i < tempContainer.size(); i++)
			if (Utilities.FindIfIp(tempContainer.get(i)))
				amount++;
		return amount;
	}

	private final int categorizeIpAttribute(int ip) {
		if (ip == 0) {
			return 0;
		}
		return 1;
	}

	private int getNumberOfMismatchingLinks(Mail mail) {
		int mockLinks = 0;
		for (int i = 0; i < mail.getHrefs().size(); i++) {
			if (mail.getTexts().get(i).contains("http")
					&& !mail.getHrefs().get(i).equalsIgnoreCase(mail.getTexts().get(i)))
				mockLinks++;
		}
		return mockLinks;
	}

	private final int categorizeMismatchingLinks(int num) {
		if (num < 3) {
			return num;
		}
		return 4;
	}

	private int getNumberOfLinks(Mail mail) {
		return extractUrls(mail).size();
	}

	private List<String> extractUrls(Mail mail) {
		List<String> result = new ArrayList<String>();
		Pattern pattern = Pattern.compile("\\b(((ht|f)tp(s?)\\:\\/\\/|~\\/|\\/)|www.)"
				+ "(\\w+:\\w+@)?(([-\\w]+\\.)+(com|org|net|gov" + "|mil|biz|info|mobi|name|aero|jobs|museum"
				+ "|travel|[a-z]{2}))(:[\\d]{1,5})?" + "(((\\/([-\\w~!$+|.,=]|%[a-f\\d]{2})+)+|\\/)+|\\?|#)?"
				+ "((\\?([-\\w~!$+|.,*:]|%[a-f\\d{2}])+=?" + "([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)"
				+ "(&(?:[-\\w~!$+|.,*:]|%[a-f\\d{2}])+=?" + "([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)*)*"
				+ "(#([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)?\\b");
		Matcher matcher = pattern.matcher(mail.getMailText());
		while (matcher.find()) {
			result.add(matcher.group());
		}
		return result;
	}

	private final int categNumOfLinks(int num) {
		if (num >= 1 && num <= 5) {
			return GlobalVars.LINKS_NUMBER_CLASS_1_5;
		}
		if (num >= 6 && num <= 10) {
			return GlobalVars.LINKS_NUMBER_CLASS_6_10;
		}
		if (num >= 11 && num <= 100) {
			return GlobalVars.LINKS_NUMBER_CLASS_10_100;
		}
		if (num > 100) {
			return GlobalVars.LINKS_NUMBER_CLASS_100_MORE;
		}
		return num;
	}

	private int getNumberOfDomainsInLinks(Mail mail) {
		ArrayList<String> ListOfDomains = new ArrayList<String>();
		ArrayList<String> tempContainer = mail.getHrefs();
		for (int i = 0; i < tempContainer.size(); i++) {
			if (ListOfDomains.contains(Utilities.cutDomainString(tempContainer.get(i))) == false) {
				ListOfDomains.add(Utilities.cutDomainString(tempContainer.get(i)));
			}
		}
		return ListOfDomains.size();
	}

	private int categNumOfDomains(int num) {
		if (num < 6) {
			return num;
		}
		if (num >= 6 && num <= 10) {
			return GlobalVars.NUMBER_OF_DOMAIN_IN_LINKS_6_10;
		}
		return GlobalVars.NUMBER_OF_DOMAIN_IN_LINKS_10_MORE;
	}

	private int getAvgDotsPerLink(Mail mail) {
		int totalAmountOfDots = 0;
		for (int i = 0; i < mail.getHrefs().size(); i++) {
			totalAmountOfDots += this.getNumberOfDotsPerLink(mail.getHrefs().get(i));
		}
		return totalAmountOfDots / (this.getNumberOfLinks() > 1 ? this.getNumberOfLinks() : 1);
	}

	private int categAvgDots(int num) {
		if (num < 4) {
			return num;
		}
		return GlobalVars.AVG_DOTS_PER_LINKS_3_MORE;
	}

	private int getNumberOfLinksWithAt(Mail mail) {
		int counter = 0;
		List<String> links = extractUrls(mail);
		Iterator<String> it = links.listIterator();
		while (it.hasNext()) {
			if (it.next().contains("@"))
				counter++;
		}
		return counter;
	}

	private int categLinksWithAt(int num) {
		if (num == 0) {
			return num;
		}
		return GlobalVars.NUMBER_OF_LINKS_WITH_AT_1_MORE;
	}

	private int getNumberOfLinksWithHex(Mail mail) {
		ArrayList<String> tempContainer = mail.getHrefs();
		int amount = 0;
		for (int i = 0; i < tempContainer.size(); i++) {
			if (Utilities.findIfHex(tempContainer.get(i)))
				amount++;
		}
		return amount;
	}

	private int categLinksWithHex(int num) {
		if (num < 6) {
			return num;
		}
		if (num >= 6 && num <= 10) {
			return GlobalVars.NUMBER_OF_LINKS_WITH_HEX_6_10;
		}
		return GlobalVars.NUMBER_OF_LINKS_WITH_HEX_10_MORE;
	}

	public int getNumberOfNonAsciiLinks(Mail mail) {
		int result = 0;
		for (int i = 0; i < mail.getHrefs().size(); i++) {
			if (Utilities.countNonAsciiCharacters(mail.getHrefs().get(i)) > 0)
				result++;
		}
		return result;
	}

	private int categNonAsciiLinks(int num) {
		if (num >= 1) {
			return GlobalVars.NUMBER_OF_LINKS_WITH_NON_ASCII_1_MORE;
		}
		return num;
	}

	private int checkIfHtml(Mail mail) {
		int index = 0;
		if (mail.getContentType().contains("text/html"))
			index += 10;
		if (mail.getMailText().contains("!DOCTYPE html PUBLIC"))
			index += 5;
		if (mail.getMailText().contains("<br>") || mail.getMailText().contains("<BR>"))
			index += 3;
		if (mail.getMailText().contains("<head>") || mail.getMailText().contains("<HEAD>"))
			index += 3;
		if (mail.getMailText().contains("<body>") || mail.getMailText().contains("<BODY>"))
			index += 3;
		if (index >= 9)
			return 1;
		return 0;
	}

	private int categMailSize(int num) {
		if (num < 1500) {
			return GlobalVars.LENGTH_CLASS_0_1000;
		}
		if (num >= 1000 && num <= 5000) {
			return GlobalVars.LENGTH_CLASS_1000_5000;
		}
		if (num > 5000 && num <= 10000) {
			return GlobalVars.LENGTH_CLASS_5000_10000;
		}
		if (num > 10000 && num <= 20000) {
			return GlobalVars.LENGTH_CLASS_10000_20000;
		}
		if (num > 20000 && num <= 30000) {
			return GlobalVars.LENGTH_CLASS_20000_30000;
		}
		if (num > 30000 && num <= 40000) {
			return GlobalVars.LENGTH_CLASS_30000_40000;
		}
		if (num > 40000 && num <= 50000) {
			return GlobalVars.LENGTH_CLASS_40000_50000;
		}
		if (num > 50000 && num <= 60000) {
			return GlobalVars.LENGTH_CLASS_50000_60000;
		}
		if (num > 60000 && num <= 70000) {
			return GlobalVars.LENGTH_CLASS_60000_70000;
		}
		if (num > 70000 && num <= 80000) {
			return GlobalVars.LENGTH_CLASS_70000_80000;
		}
		if (num > 80000 && num <= 90000) {
			return GlobalVars.LENGTH_CLASS_80000_90000;
		}
		if (num > 90000 && num <= 100000) {
			return GlobalVars.LENGTH_CLASS_90000_100000;
		}
		return GlobalVars.LENGTH_CLASS_100000_MORE;
	}

	private int getAttachmentNumber(Mail mail) {
		int lastIndex = 0;
		int count = 0;

		while (lastIndex != -1) {

			lastIndex = mail.getMailText().indexOf("Content-Disposition: attachment", lastIndex);

			if (lastIndex != -1) {
				count++;
				lastIndex += "Content-Disposition: attachment".length();
			}
		}
		return count;
	}

	private int categNumAttach(int num) {
		if (num < 4) {
			return num;
		}
		return GlobalVars.NUMBER_OF_ATTACHMENTS_4_MORE;
	}

	private int retrieveAttSize(Mail mail) {
		int finalResult = 0;
		int avgsize = 0;
		int[] sizes = getAttachmentSizes(mail);
		for (int i = 0; i < sizes.length; i++) {
			avgsize += sizes[i];
			avgsize = avgsize / sizes.length;
			finalResult += Utilities.categAttSize(avgsize);
		}
		return finalResult;
	}

	public int[] getAttachmentSizes(Mail mail) {
		int lastIndex = 0;
		int[] result = new int[getAttachmentNumber(mail)];
		lastIndex = mail.getMailText().indexOf("Content-Disposition: attachment", lastIndex);
		String afterAttachment = mail.getMailText().substring(lastIndex + "Content-Disposition: attachment".length());
		int startAttachment = afterAttachment.indexOf("\"", "filename".length() + 4);
		for (int i = 0; i < result.length; i++) {
			if (afterAttachment.indexOf("Content-Type", startAttachment) != -1) {
				String attachment = afterAttachment.substring(startAttachment,
						afterAttachment.indexOf("Content-Type", startAttachment));
				int size = attachment.length();
				result[i] = size;
				afterAttachment = afterAttachment.substring(afterAttachment.indexOf("Content-Type", startAttachment));
			}
		}
		return result;
	}

	private int categAttSize(int num) {
		if (num == 0) {
			return num;
		}
		if (num >= 1 && num <= 100) {
			return GlobalVars.ATTACHMENT_SIZE_1_100;
		}
		if (num > 100 && num <= 500) {
			return GlobalVars.ATTACHMENT_SIZE_100_500;
		}
		if (num > 500 && num <= 1000) {
			return GlobalVars.ATTACHMENT_SIZE_500_1000;
		}
		return GlobalVars.ATTACHMENT_SIZE_1000_MORE;
	}

	private String[] getAttachmentType(Mail mail) {
		String[] attachmentsTypes = new String[getAttachmentNumber(mail)];
		int lastIndex = 0;
		for (int i = 0; i < this.getAttachmentNumber(mail); i++) {
			lastIndex = mail.getMailText().indexOf("Content-Disposition: attachment", lastIndex);
			try {
				String afterAttachment = mail.getMailText()
						.substring(lastIndex + "Content-Disposition: attachment".length());
				String filename = afterAttachment.substring(Math.max(afterAttachment.indexOf("\"") + 1, 1),
						Math.max(afterAttachment.indexOf("\"", "filename".length() + 4), 4));
				String extension = filename.substring(filename.lastIndexOf(".") + 1);
				attachmentsTypes[i] = extension;
			} catch (StringIndexOutOfBoundsException se) {
				System.out.println("Unable to extract the attachment type");
				attachmentsTypes[i] = "NONE";
			}
		}
		return attachmentsTypes;
	}

	public static int categAttachmentType(String type) {
		if (type.equalsIgnoreCase("pdf"))
			return GlobalVars.PDF;
		if (type.equalsIgnoreCase("exe"))
			return GlobalVars.EXEC;
		if (type.equalsIgnoreCase("doc") || type.equalsIgnoreCase("docx"))
			return GlobalVars.DOC;
		if (type.equalsIgnoreCase("png") || type.equalsIgnoreCase("jpg") || type.equalsIgnoreCase("jpeg")
				|| type.equalsIgnoreCase("bmp") || type.equalsIgnoreCase("gif"))
			return GlobalVars.PIC;
		if (type.equalsIgnoreCase("zip") || type.equalsIgnoreCase("rar") || type.equalsIgnoreCase("7z")
				|| type.equalsIgnoreCase("tar") || type.equalsIgnoreCase("bz"))
			return GlobalVars.ZIP;
		if (type.equalsIgnoreCase("txt"))
			return GlobalVars.TXT;
		return GlobalVars.OTHER;
	}

	private int categNumWordSubj(int num) {
		if (num == 0) {
			return num;
		}
		if (num >= 1 && num <= 5) {
			return GlobalVars.NUMBER_OF_WORDS_IN_SUBJECT_1_5;
		}
		if (num >= 6 && num <= 10) {
			return GlobalVars.NUMBER_OF_WORDS_IN_SUBJECT_6_10;
		}
		return GlobalVars.NUMBER_OF_WORDS_IN_SUBJECT_10_MORE;
	}

	private int categNumCharsInSubj(int num) {
		if (num == 0) {
			return num;
		}
		if (num >= 1 && num <= 10) {
			return GlobalVars.NUMBER_OF_CHARS_IN_SUBJECT_1_10;
		}
		if (num >= 10 && num <= 20) {
			return GlobalVars.NUMBER_OF_CHARS_IN_SUBJECT_10_20;
		}
		return GlobalVars.NUMBER_OF_CHARS_IN_SUBJECT_20_MORE;
	}

	private int categNumImages(int num) {
		if (num == 0) {
			return num;
		}
		if (num >= 1 && num <= 10) {
			return GlobalVars.IMG_NUMBER_1_10;
		}
		if (num > 10 && num <= 20) {
			return GlobalVars.IMG_NUMBER_10_20;
		}
		if (num > 20 && num <= 30) {
			return GlobalVars.IMG_NUMBER_20_30;
		}
		if (num > 30 && num <= 40) {
			return GlobalVars.IMG_NUMBER_30_40;
		}
		if (num > 40 && num <= 50) {
			return GlobalVars.IMG_NUMBER_40_50;
		}
		if (num > 50 && num <= 100) {
			return GlobalVars.IMG_NUMBER_50_100;
		}
		if (num > 100 && num <= 500) {
			return GlobalVars.IMG_NUMBER_100_500;
		}
		if (num > 500 && num <= 1000) {
			return GlobalVars.IMG_NUMBER_500_1000;
		}
		return GlobalVars.IMG_NUMBER_1000_MORE;
	}

	private int categNumNonAsciiCharInSubj(int num) {
		if (num < 2) {
			return num;
		}
		if (num > 2 && num <= 5) {
			return GlobalVars.NO_ASCII_CHAR_IN_SUBJECT_2_5;
		}
		if (num > 5 && num <= 10) {
			return GlobalVars.NO_ASCII_CHAR_IN_SUBJECT_5_10;
		}
		return GlobalVars.NO_ASCII_CHAR_IN_SUBJECT_10_MORE;
	}

	public int findReOrFwdInSubject(String subject) {
		if (subject.contains("Re:") || subject.contains("FW") || subject.contains("Fwd") || subject.contains("FWD")
				|| subject.contains("RE"))
			return 1;
		return 0;
	}

	private int categCountRecipients(int num) {
		if (num < 2) {
			return num;
		}
		return GlobalVars.RECIPIENTS_NUMBER_1_MORE;
	}

	public InputStream getFilePath() {
		return filePath;
	}

	public void setFilePath(InputStream filePath) {
		this.filePath = new BufferedInputStream(filePath);
	}

	public String getLineFromMail(String element) {
		String tempLine = "";
		String result = "";
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new ByteArrayInputStream(content.getBytes())))) {
			tempLine = br.readLine();
			StringTokenizer st = new StringTokenizer(tempLine, ":");
			while (!st.nextToken().contentEquals(element)) {
				tempLine = br.readLine();
			}
			st = new StringTokenizer(tempLine, ":");

			result = tempLine;

			return result;
		} catch (IOException | NoSuchElementException ne) {
			ne.printStackTrace();
			System.out.println("MISSING ELEMENT: " + element);
			return "MISSING";
		}

	}

	public String getFromMail(String content, String element) {

		String tempLine = "";
		String result = "";
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new ByteArrayInputStream(content.getBytes())))) {

			tempLine = br.readLine();
			StringTokenizer st;
			while ((tempLine = br.readLine()) != null) {
				if (tempLine.contains(":")) {
					st = new StringTokenizer(tempLine, ":");
					String currentToken = st.nextToken();

					if (currentToken.equals(element)) {
						result = st.nextToken().trim();
						System.out.println(result);

						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "NOT_FOUND";
		}

		return result;
	}

	public String lookForMessageType(String content) {
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new ByteArrayInputStream(content.getBytes())))) {
			String tempLine = (br.readLine() != null) ? br.readLine() : "";
			StringTokenizer st = new StringTokenizer(tempLine, ":");
			while (!st.nextToken().contentEquals("Content-Type")) {
				tempLine = br.readLine();
				st = new StringTokenizer(tempLine, ":");
			}
			return st.nextToken().trim();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getMailText(String content) {
		String tempLine = "";
		StringBuilder buffer = new StringBuilder();
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new ByteArrayInputStream(content.getBytes())))) {
			tempLine = br.readLine();
			StringTokenizer st = new StringTokenizer(tempLine, ":");
			while (!st.nextToken().contentEquals("Content-Length")) {
				tempLine = br.readLine();
				if (tempLine == null || tempLine.length() == 0) {
					break;
				}
				st = new StringTokenizer(tempLine, ":");
			}
			while ((tempLine = br.readLine()) != null) {
				buffer.append(tempLine);
			}
			return buffer.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean CheckIfHtml() {
		int index = 0;
		if (mail.getContentType().contains("text/html"))
			index += 10;
		if (mail.getMailText().contains("!DOCTYPE html PUBLIC"))
			index += 5;
		if (mail.getMailText().contains("<br>") || mail.getMailText().contains("<BR>"))
			index += 3;
		if (mail.getMailText().contains("<head>") || mail.getMailText().contains("<HEAD>"))
			index += 3;
		if (mail.getMailText().contains("<body>") || mail.getMailText().contains("<BODY>"))
			index += 3;
		if (index >= 9)
			return true;
		return false;
	}

	public boolean CheckIfLinkOnly() {
		int index = 0;
		try {
			if (Utilities.countLines(this.filePath) < 25)
				index += 0.5;
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (mail.getMailText().contains("http://") || mail.getMailText().contains("www")
				|| mail.getMailText().contains("https://"))
			index += 0.5;
		if (index >= 1) {
			return true;
		}
		return false;
	}

	public boolean CheckForImages(int threshold) {
		int amount = Utilities.countOccurencies("img src", mail);
		if (amount >= threshold)
			return true;
		return false;
	}

	public List<String> extractUrls() {
		List<String> result = new ArrayList<String>();
		Pattern pattern = Pattern.compile("\\b(((ht|f)tp(s?)\\:\\/\\/|~\\/|\\/)|www.)"
				+ "(\\w+:\\w+@)?(([-\\w]+\\.)+(com|org|net|gov" + "|mil|biz|info|mobi|name|aero|jobs|museum"
				+ "|travel|[a-z]{2}))(:[\\d]{1,5})?" + "(((\\/([-\\w~!$+|.,=]|%[a-f\\d]{2})+)+|\\/)+|\\?|#)?"
				+ "((\\?([-\\w~!$+|.,*:]|%[a-f\\d{2}])+=?" + "([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)"
				+ "(&(?:[-\\w~!$+|.,*:]|%[a-f\\d{2}])+=?" + "([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)*)*"
				+ "(#([-\\w~!$+|.,*:=]|%[a-f\\d]{2})*)?\\b");
		Matcher matcher = pattern.matcher(this.getMail().getMailText());
		while (matcher.find()) {
			result.add(matcher.group());
		}
		return result;
	}

	private int getNumberOfLinks() {
		return extractUrls().size();
	}

	private int getNumberOfDotsPerLink(String link) {
		int counter = 0;
		for (int i = 0; i < link.length(); i++) {
			if (link.charAt(i) == '.') {
				counter++;
			}
		}
		return counter;
	}

	private ArrayList<String> getAllHref(String content) {
		Document doc = null;
		doc = Jsoup.parse(content, "UTF-8");
		Elements links = doc.getElementsByTag("a");
		ArrayList<String> hrefs = new ArrayList<String>();
		for (Element link : links) {
			hrefs.add(link.attr("href"));
		}
		return hrefs;
	}

	private ArrayList<String> getAllTextInHref(String content) {
		Document doc = null;
		doc = Jsoup.parse(content, "UTF-8");
		Elements links = doc.getElementsByTag("a");
		ArrayList<String> texts = new ArrayList<String>();
		for (Element link : links) {
			texts.add(link.text());

		}
		return texts;
	}

	public int getEmailSize() {
		return mail.getSize();
	}

	public static Point getPositionFromIp(String ip) {
		Point point = null;
		/*
		 * try { // TODO do not open each time String dbfile =
		 * ResourceUtils.getFile("geoip_db/GeoLiteCity.dat") .getAbsolutePath();
		 * LOGGER.info("DBFile: " + dbfile); if (dbfile == null || dbfile.isEmpty()) {
		 * return null; } LookupService cl = new LookupService(dbfile,
		 * LookupService.GEOIP_STANDARD); // LookupService.GEOIP_MEMORY_CACHE | //
		 * LookupService.GEOIP_CHECK_CACHE); Location location = cl.getLocation(ip); if
		 * (location != null) { point = new Point();
		 * point.setLatitude(location.latitude); point.setLongitude(location.longitude);
		 * return point; } else { return null; }
		 * 
		 * } catch (Exception e) { e.printStackTrace(); return null; }
		 */
		return null;

	}

	private boolean isPrivateIP(String ipAddress) {
		boolean isValid = false;

		if (ipAddress != null && !ipAddress.isEmpty()) {
			String[] ip = ipAddress.split("\\.");
			short[] ipNumber = new short[] { Short.parseShort(ip[0]), Short.parseShort(ip[1]), Short.parseShort(ip[2]),
					Short.parseShort(ip[3]) };

			if (ipNumber[0] == 10) { // Class A
				isValid = true;
			} else if (ipNumber[0] == 172 && (ipNumber[1] >= 16 && ipNumber[1] <= 31)) { // Class B
				isValid = true;
			} else if (ipNumber[0] == 192 && ipNumber[1] == 168) { // Class C
				isValid = true;
			} else if (ipNumber[0] == 127 && ipNumber[1] == 0 && ipNumber[2] == 0 && ipNumber[3] == 1) { // loopback
				isValid = true;
			}
		}
		return isValid;
	}

}
