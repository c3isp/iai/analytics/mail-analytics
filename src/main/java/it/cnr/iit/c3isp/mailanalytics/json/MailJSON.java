package it.cnr.iit.c3isp.mailanalytics.json;

import java.util.List;

/**
 * This class represents the JSON of the mail to be passed to the map.
 * <p>
 * 
 * </p>
 * 
 * @author antonio
 *
 */
final public class MailJSON {
  // list of arcs in the world
  Arcs arcs = new Arcs();

  public Arcs getArcs() {
    return arcs;
  }

  public void setArcs(List<Arc> arcs) {
    this.arcs.setArcs(arcs);
  }

  public void addArc(Arc arc) {
    arcs.addArc(arc);
  }

}
