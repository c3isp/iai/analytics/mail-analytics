package it.cnr.iit.c3isp.mailanalytics.json;

import java.util.ArrayList;
import java.util.List;

public class Arcs {
  private List<Arc> arcs = new ArrayList<>();

  public List<Arc> getArcs() {
    return arcs;
  }

  public void setArcs(List<Arc> arcs) {
    this.arcs = arcs;
  }

  protected void addArc(Arc arc) {
    // BEGIN parameter checking
    // END parameter checking
    boolean added = false;
    for (Arc tmpArc : arcs) {
      if (tmpArc.getId().equals(arc.getId())) {
        tmpArc.incrementCounter();
        added = true;
        break;
      }
    }
    if (added == false && arcs.size() < 50) {
      arcs.add(arc);
    }
  }
}
