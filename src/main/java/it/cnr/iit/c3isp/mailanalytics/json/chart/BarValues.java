package it.cnr.iit.c3isp.mailanalytics.json.chart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BarValues {

  @JsonProperty("label")
  String label = "";
  @JsonProperty("value")
  int value = 0;

  public BarValues() {

  }

  public void setLabel(String label) {
    this.label = label;
  }

  public void setValue(int value) {
    this.value = value;
  }
}
