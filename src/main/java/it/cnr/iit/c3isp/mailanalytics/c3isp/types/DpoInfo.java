package it.cnr.iit.c3isp.mailanalytics.c3isp.types;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Part of the informations about the DPO returned by the ISI-API when calling
 * the prepare data
 * 
 * @author antonio
 *
 */
public class DpoInfo {
  @JsonProperty("DPO_id")
  private String DPO_id;
  @JsonProperty("code")
  private String code;
  @JsonProperty("file")
  private String file;
  @JsonProperty("message")
  private String message;

  public DpoInfo() {

  }

  public String getDPO_id() {
    return DPO_id;
  }

  public void setDPO_id(String dPO_id) {
    DPO_id = dPO_id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getFile() {
    return file;
  }

  public void setFile(String file) {
    this.file = file;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}
