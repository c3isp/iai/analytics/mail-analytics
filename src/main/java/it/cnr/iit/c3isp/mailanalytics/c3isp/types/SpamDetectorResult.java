package it.cnr.iit.c3isp.mailanalytics.c3isp.types;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SpamDetectorResult {
	@JsonProperty(value = "filename")
	private String fileName;
	
	@JsonProperty(value = "dpo")
	private String dpo;

	@JsonProperty(value = "spamProbability")
	private String spamProbability;

	@JsonProperty(value = "sender")
	private String sender;

	@JsonProperty(value = "receipt")
	private String receipt;

	@JsonProperty(value = "subject")
	private String subject;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public void setDpo(String dpo) {
		this.dpo = dpo;
	}
	
	public String getDpo() {
		return dpo;
	}

	public String getSpamProbability() {
		return spamProbability;
	}

	public void setSpamProbability(String spamProbability) {
		this.spamProbability = spamProbability;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceipt() {
		return receipt;
	}

	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}

}
