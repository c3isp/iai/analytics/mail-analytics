package it.cnr.iit.c3isp.mailanalytics.json.chart;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.c3isp.mailanalytics.json.ClassForEmailID;
import it.cnr.iit.c3isp.mailanalytics.json.ClassificationResult;

public class BarChart {
	String key = "";
	@JsonProperty("values")
	List<BarValues> values = new ArrayList<BarValues>();

	public BarChart() {

	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void addBarValue(BarValues barValue) {
		values.add(barValue);
	}

	public static BarChart convertClassificationResultToBarChart(ClassificationResult classificationResult) {
		HashMap<String, Integer> emailPerLabel = new HashMap<>();
		for (ClassForEmailID classifiedEmail : classificationResult.getList()) {
			if (!emailPerLabel.containsKey(classifiedEmail.getClassifiedAs())) {
				emailPerLabel.put(classifiedEmail.getClassifiedAs(), 0);
			}
			emailPerLabel.put(classifiedEmail.getClassifiedAs(),
					emailPerLabel.get(classifiedEmail.getClassifiedAs()) + 1);
		}
		BarChart barChart = new BarChart();
		for (Map.Entry<String, Integer> entry : emailPerLabel.entrySet()) {
			BarValues values = new BarValues();
			values.setLabel(entry.getKey());
			values.setValue(entry.getValue());
			barChart.values.add(values);
		}

		return barChart;
	}

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (IOException exception) {
			exception.printStackTrace();
			return null;
		}
	}

	public List<BarValues> getValues() {
		return values;
	}

	public void setValues(List<BarValues> values) {
		this.values = values;
	}

}
