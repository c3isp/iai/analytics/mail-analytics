package it.cnr.iit.c3isp.mailanalytics.async;

import java.util.HashMap;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.clusterer.cctree.CCTree;
import it.cnr.iit.c3isp.mailanalytics.json.ClassificationResult;
import it.cnr.iit.c3isp.mailanalytics.utils.AnalyticUtils;
import it.cnr.iit.common.analytic.AbstractAnalyticExecutor;

@Service
public class AnalyticExecutor extends AbstractAnalyticExecutor {

	public static final String CLASSIFY_ANALYTIC = "classify";
	public static final String CLASSIFY_CLUSTERER = "clusterer";
	public static final String CLASSIFY_DETECT = "detect";

	private final static Logger log = Logger.getLogger(AnalyticExecutor.class.getName());

	@Async
	@Override
	@SuppressWarnings("unchecked")
	public Future<String> executeWithResult(Object... objects) {
		try {
//			log.severe("sleeping 30 sec...");
//			Thread.sleep(30 * 1000);
			log.severe("awaking!");
			String analytic = (String) objects[0];
			AnalyticUtils analyticUtils = (AnalyticUtils) objects[1];
			if (analytic.equals(CLASSIFY_ANALYTIC)) {
				HashMap<String, String> mailIdInDataset = (HashMap<String, String>) objects[2];
				HashMap<String, HashMap<String, String>> mailAttributes = (HashMap<String, HashMap<String, String>>) objects[3];
				DataSet<Integer> testdataset = (DataSet<Integer>) objects[4];
				ClassificationResult result = analyticUtils.classify(testdataset, mailIdInDataset, mailAttributes);
				String outputFile = analyticUtils.createFileFromString(analytic, result.toString());
				// returns file with results
				return new AsyncResult<String>(outputFile);
			} else if (analytic.equals(CLASSIFY_CLUSTERER)) {
				CCTree cctree = (CCTree) objects[2];
				HashMap<String, HashMap<String, String>> mailAttributes = (HashMap<String, HashMap<String, String>>) objects[3];
				DataSet<Integer> testdataset = (DataSet<Integer>) objects[4];
				ClassificationResult result = analyticUtils.clusterer(cctree, mailAttributes, testdataset);
				String outputFile = analyticUtils.createFileFromString(analytic, result.toString());
				return new AsyncResult<String>(outputFile);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new AsyncResult<String>("ERROR: " + e.getMessage());
		}
		return null;
	}

	@Override
	protected <T> T executeWithResult() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void execute(Object... objects) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void execute() {
		// TODO Auto-generated method stub

	}
}
