package it.cnr.iit.c3isp.mailanalytics.json.chart;

import java.util.ArrayList;
import java.util.List;

public class MultiBarChart {
  String key = "";
  boolean nonStackable = true;
  List<StackedValue> values = new ArrayList<StackedValue>();

  public MultiBarChart() {

  }

  public void setKey(String key) {
    this.key = key;
  }

  public void setStackable(boolean stackable) {
    nonStackable = stackable;
  }

  public void setValues(StackedValue value) {
    this.values.add(value);
  }

  public List<StackedValue> getValues() {
    return this.values;
  }

  public String getKey() {
    return this.key;
  }

  public void setValueOnTime(String timestamp, int value) {
    for (int i = 0; i < this.values.size(); i++) {
      System.out.println(timestamp + " " + this.values.get(i).x);
      if (this.values.get(i).getX().equals(timestamp))
        this.values.get(i).setY(value);
    }
  }
}
