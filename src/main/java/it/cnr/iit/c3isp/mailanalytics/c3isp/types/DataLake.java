package it.cnr.iit.c3isp.mailanalytics.c3isp.types;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Informations about the datalake returned in the DataLakeBufferReturn
 * 
 * @author antonio
 *
 */
public class DataLake {
  @JsonProperty("URI")
  private String URI;
  @JsonProperty("message")
  private String message;

  public DataLake() {

  }

  public String getURI() {
    return URI;
  }

  public void setURI(String uRI) {
    URI = uRI;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}
