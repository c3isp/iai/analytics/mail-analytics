package it.cnr.iit.c3isp.mailanalytics.json;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.cnr.iit.c3isp.mailanalytics.c3isp.types.DataLakeBuffer;
import it.cnr.iit.c3isp.mailanalytics.c3isp.types.SpamDetectorResult;

public class ClassificationResult {

	private List<ClassForEmailID> list = new ArrayList<>();

	public List<ClassForEmailID> getList() {
		return list;
	}

	public void setList(List<ClassForEmailID> list) {
		this.list = list;
	}

	public boolean addClassifiedEmail(ClassForEmailID classifiedMail) {
//		if (!utility.verifyArgument(classifiedMail)) {
//			return false;
//		}
		return list.add(classifiedMail);
	}

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static ClassificationResult createFromSpamDetect(String result, DataLakeBuffer datalake) {

		try {
			SpamDetectorResult[] spamDetectorResults = new ObjectMapper().readValue(result, SpamDetectorResult[].class);
			ClassificationResult classificationResult = new ClassificationResult();
			for (SpamDetectorResult sdResult : spamDetectorResults) {
				ClassForEmailID label = new ClassForEmailID();

				String dpoId = "<unknown>";
				URI uri;
				try {
					uri = new URI(sdResult.getDpo());
					String[] segments = uri.getPath().split("/");
					dpoId = segments[segments.length - 1];
					dpoId = dpoId.split(".dpo")[0];
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}

				label.setMailId(dpoId);
				label.setFilename(sdResult.getFileName());
				label.setClassifiedAs(sdResult.getSpamProbability());
				label.setDestination(sdResult.getReceipt());
				label.setSource(sdResult.getSender());
				label.setSubject(sdResult.getSubject());
				classificationResult.addClassifiedEmail(label);
			}
			return classificationResult;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

}
