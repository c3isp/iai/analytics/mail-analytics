package it.cnr.iit.c3isp.mailanalytics.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClassForEmailID {
	@JsonProperty("classifiedAs")
	private String classifiedAs;
	@JsonProperty("mailId")
	private String mailId;
	@JsonProperty("source")
	private String source;
	@JsonProperty("destination")
	private String destination;
	@JsonProperty("subject")
	private String subject;
	@JsonProperty("purity")
	private String purity;
	@JsonProperty("leafId")
	private String leafId;
	@JsonProperty("filename")
	private String filename;


	public String getPurity() {
		return purity;
	}

	public void setPurity(String purity) {
		this.purity = purity;
	}
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public String getLeafId() {
		return leafId;
	}

	public void setLeafId(String leafId) {
		this.leafId = leafId;
	}
	
	public String getClassifiedAs() {
		return classifiedAs;
	}

	public void setClassifiedAs(String classifiedAs) {
		this.classifiedAs = classifiedAs;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}
