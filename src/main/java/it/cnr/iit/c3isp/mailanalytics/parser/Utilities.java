package it.cnr.iit.c3isp.mailanalytics.parser;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;

public class Utilities {
  public static String HexRegEx = "[a-fA-F0-9]{4,}";

  private static String getLanguage(String filePath) {
    String langResult = "";
    BufferedReader in = null;
    try {
      Process proc = Runtime.getRuntime().exec("python lid.py " + filePath);
      in = new BufferedReader(new InputStreamReader(proc.getInputStream()));

    } catch (IOException e) {
      System.out.println("WARNING: Cannot use Python, using default language.");
      return "unknown";
    }
    try {

      String tempString;
      while ((tempString = in.readLine()) != null)
        langResult += tempString;

      System.out.println(langResult);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    StringTokenizer st = new StringTokenizer(langResult, ":");
    try {
      st.nextToken();
      st.nextToken();
    } catch (NoSuchElementException e) {
      System.out.println("Cannot parse language string");
      return "unknown";
    }
    return st.nextToken().trim();
  }

  public static String FromStringToFile(String myText) {
    try {
      PrintWriter out = new PrintWriter(GlobalVars.tempFile);
      out.println(myText);
      out.close();
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return GlobalVars.tempFile;
  }

  // WARNING: Always keep this function synchronized with the GlobalVars list
  // of languages
  public static void SetLanguageFromString(String lang, Mail m) {
    if (lang.contentEquals("English")) {
      m.setLanguage(GlobalVars.ITALIAN_LANG);
      return;
    }

    if (lang.contentEquals("Italian")) {
      m.setLanguage(GlobalVars.ITALIAN_LANG);
      return;
    }

    if (lang.contentEquals("French")) {
      m.setLanguage(GlobalVars.FRENCH_LANG);
      return;
    }
    if (lang.contentEquals("German")) {
      m.setLanguage(GlobalVars.GERMAN_LANG);
      return;
    }
    if (lang.contentEquals("Spanish")) {
      m.setLanguage(GlobalVars.SPANISH_LANG);
      return;
    }
    if (lang.contentEquals("Chinese")) {
      m.setLanguage(GlobalVars.CHINESE_LANG);
      return;
    }
    if (lang.contentEquals("Arab")) {
      m.setLanguage(GlobalVars.ARAB_LANG);
      return;
    }

    if (lang.contentEquals("Persian")) {
      m.setLanguage(GlobalVars.PERSIAN_LANG);
      return;
    }
    if (lang.contentEquals("Japan")) {
      m.setLanguage(GlobalVars.JAPAN_LANG);
      return;
    }
    if (lang.contentEquals("Croatian")) {
      m.setLanguage(GlobalVars.CROATIAN_LANG);
      return;
    }
    m.setLanguage(GlobalVars.UNKNOWN_LANG);

  }

  public static int countLines(String filename) throws IOException {
    InputStream is = new BufferedInputStream(new FileInputStream(filename));
    try {
      byte[] c = new byte[1024];
      int count = 0;
      int readChars = 0;
      boolean empty = true;
      while ((readChars = is.read(c)) != -1) {
        empty = false;
        for (int i = 0; i < readChars; ++i) {
          if (c[i] == '\n') {
            ++count;
          }
        }
      }
      return (count == 0 && !empty) ? 1 : count;
    } finally {
      is.close();
    }
  }

  public static int countLines(InputStream filename) throws IOException {
    InputStream is = filename;
    try {
      byte[] c = new byte[1024];
      int count = 0;
      int readChars = 0;
      boolean empty = true;
      while ((readChars = is.read(c)) != -1) {
        empty = false;
        for (int i = 0; i < readChars; ++i) {
          if (c[i] == '\n') {
            ++count;
          }
        }
      }
      return (count == 0 && !empty) ? 1 : count;
    } finally {
      is.close();
    }
  }

  public static int countOccurencies(String substr, Mail m) {
    int lastIndex = 0;
    int count = 0;
    while (lastIndex != -1) {
      lastIndex = m.getMailText().indexOf(substr, lastIndex);
      if (lastIndex != -1) {
        count++;
        lastIndex += substr.length();
      }
    }
    return count;
  }

  public static String html2text(String html) {
    return Jsoup.parse(html).text();
  }

  public static boolean FindIfIp(String s) {
    Pattern pattern = Pattern.compile(GlobalVars.IPADDRESS_PATTERN);
    Matcher matcher = pattern.matcher(s);
    if (matcher.find()) {
      return true;
    } else {
      return false;
    }
  }

  public static int countNonAsciiCharacters(String s) {
    Pattern pattern = Pattern.compile(GlobalVars.ASCII_PATTERN);
    Matcher matcher = pattern.matcher(s);
    int count = 0;
    while (matcher.find())
      count++;
    return count;
  }

  public static String cutDomainString(String link) {
    String temp = link.substring(Math.min(7, link.length()), link.length());
    int endDominium = temp.indexOf("/");
    if (endDominium == -1)
      endDominium = temp.length();
    return temp.substring(0, endDominium);
  }

  public static boolean matchWordList(String link, String[] keywords) {
    for (int i = 0; i < keywords.length; i++)
      if (link.contains(keywords[i]))
        return true;
    return false;
  }

  public static boolean findIfHex(String s) {
    Pattern pattern = Pattern.compile(Utilities.HexRegEx);
    Matcher matcher = pattern.matcher(s);
    if (matcher.find())
      return true;
    else
      return false;
  }

  public static int wordCount(String text) {
    String trimmed = text.trim();
    int words = trimmed.isEmpty() ? 0 : trimmed.split("\\s+").length;
    return words;
  }

  public static int getAttachmentType(String type) {
    if (type.equalsIgnoreCase("pdf"))
      return GlobalVars.PDF;
    if (type.equalsIgnoreCase("exe"))
      return GlobalVars.EXEC;
    if (type.equalsIgnoreCase("doc") || type.equalsIgnoreCase("docx"))
      return GlobalVars.DOC;
    if (type.equalsIgnoreCase("png") || type.equalsIgnoreCase("jpg")
        || type.equalsIgnoreCase("jpeg") || type.equalsIgnoreCase("bmp")
        || type.equalsIgnoreCase("gif"))
      return GlobalVars.PIC;
    if (type.equalsIgnoreCase("zip") || type.equalsIgnoreCase("rar")
        || type.equalsIgnoreCase("7z") || type.equalsIgnoreCase("tar")
        || type.equalsIgnoreCase("bz"))
      return GlobalVars.ZIP;
    if (type.equalsIgnoreCase("txt"))
      return GlobalVars.TXT;
    return GlobalVars.OTHER;
  }

  public static int countEmailsInLine(String line) {
    int count = line.length() - line.replace("@", "").length();
    return count;
  }

  public static String[] readAllFilesInFolder(String path) {
    File folder = new File(path);
    File[] listOfFiles = folder.listFiles();
    String[] result = new String[listOfFiles.length];
    for (int i = 0; i < listOfFiles.length; i++) {
      if (listOfFiles[i].isFile()) {
        System.out.println("File " + listOfFiles[i].getName());
        result[i] = listOfFiles[i].getName();
      } else if (listOfFiles[i].isDirectory()) {
        System.out.println("Directory " + listOfFiles[i].getName());
      }
    }
    return result;
  }

  public final static int categIPBasedNumber(int ip) {
    if (ip == 0)
      return 0;
    return 1;
  }

  public final static int categMismatching(int num) {
    if (num < 3)
      return num;
    return 4;
  }

  public final static int categNumOfLinks(int num) {
    if (num >= 1 && num <= 5)
      return GlobalVars.LINKS_NUMBER_CLASS_1_5;
    if (num >= 6 && num <= 10)
      return GlobalVars.LINKS_NUMBER_CLASS_6_10;
    if (num >= 11 && num <= 100)
      return GlobalVars.LINKS_NUMBER_CLASS_10_100;
    if (num > 100)
      return GlobalVars.LINKS_NUMBER_CLASS_100_MORE;
    return num;
  }

  public final static int categNumOfDomains(int num) {
    if (num < 6)
      return num;
    if (num >= 6 && num <= 10)
      return GlobalVars.NUMBER_OF_DOMAIN_IN_LINKS_6_10;
    return GlobalVars.NUMBER_OF_DOMAIN_IN_LINKS_10_MORE;
  }

  public final static int categAvgDots(int num) {
    if (num < 4)
      return num;
    return GlobalVars.AVG_DOTS_PER_LINKS_3_MORE;
  }

  public final static int categLinksWithAt(int num) {
    if (num == 0)
      return num;
    return GlobalVars.NUMBER_OF_LINKS_WITH_AT_1_MORE;
  }

  public final static int categLinksWithHex(int num) {
    if (num < 6)
      return num;
    if (num >= 6 && num <= 10)
      return GlobalVars.NUMBER_OF_LINKS_WITH_HEX_6_10;
    return GlobalVars.NUMBER_OF_LINKS_WITH_HEX_10_MORE;
  }

  public final static int categNonAsciiLinks(int num) {
    if (num >= 1)
      return GlobalVars.NUMBER_OF_LINKS_WITH_NON_ASCII_1_MORE;
    return num;
  }

  public final static int categMailSize(int num) {
    if (num < 1500)
      return GlobalVars.LENGTH_CLASS_0_1000;
    if (num >= 1000 && num <= 5000)
      return GlobalVars.LENGTH_CLASS_1000_5000;
    if (num > 5000 && num <= 10000)
      return GlobalVars.LENGTH_CLASS_5000_10000;
    if (num > 10000 && num <= 20000)
      return GlobalVars.LENGTH_CLASS_10000_20000;
    if (num > 20000 && num <= 30000)
      return GlobalVars.LENGTH_CLASS_20000_30000;
    if (num > 30000 && num <= 40000)
      return GlobalVars.LENGTH_CLASS_30000_40000;
    if (num > 40000 && num <= 50000)
      return GlobalVars.LENGTH_CLASS_40000_50000;
    if (num > 50000 && num <= 60000)
      return GlobalVars.LENGTH_CLASS_50000_60000;
    if (num > 60000 && num <= 70000)
      return GlobalVars.LENGTH_CLASS_60000_70000;
    if (num > 70000 && num <= 80000)
      return GlobalVars.LENGTH_CLASS_70000_80000;
    if (num > 80000 && num <= 90000)
      return GlobalVars.LENGTH_CLASS_80000_90000;
    if (num > 90000 && num <= 100000)
      return GlobalVars.LENGTH_CLASS_90000_100000;
    return GlobalVars.LENGTH_CLASS_100000_MORE;
  }

  public final static int categNumAttach(int num) {
    if (num < 4)
      return num;
    return GlobalVars.NUMBER_OF_ATTACHMENTS_4_MORE;
  }

  public final static int categNumWordSubj(int num) {
    if (num == 0)
      return num;
    if (num >= 1 && num <= 5)
      return GlobalVars.NUMBER_OF_WORDS_IN_SUBJECT_1_5;
    if (num >= 6 && num <= 10)
      return GlobalVars.NUMBER_OF_WORDS_IN_SUBJECT_6_10;
    return GlobalVars.NUMBER_OF_WORDS_IN_SUBJECT_10_MORE;
  }

  public final static int categNumCharsInSubj(int num) {
    if (num == 0)
      return num;
    if (num >= 1 && num <= 10)
      return GlobalVars.NUMBER_OF_CHARS_IN_SUBJECT_1_10;
    if (num >= 10 && num <= 20)
      return GlobalVars.NUMBER_OF_CHARS_IN_SUBJECT_10_20;
    return GlobalVars.NUMBER_OF_CHARS_IN_SUBJECT_20_MORE;
  }

  public final static int categNumImages(int num) {
    if (num == 0)
      return num;
    if (num >= 1 && num <= 10)
      return GlobalVars.IMG_NUMBER_1_10;
    if (num > 10 && num <= 20)
      return GlobalVars.IMG_NUMBER_10_20;
    if (num > 20 && num <= 30)
      return GlobalVars.IMG_NUMBER_20_30;
    if (num > 30 && num <= 40)
      return GlobalVars.IMG_NUMBER_30_40;
    if (num > 40 && num <= 50)
      return GlobalVars.IMG_NUMBER_40_50;
    if (num > 50 && num <= 100)
      return GlobalVars.IMG_NUMBER_50_100;
    if (num > 100 && num <= 500)
      return GlobalVars.IMG_NUMBER_100_500;
    if (num > 500 && num <= 1000)
      return GlobalVars.IMG_NUMBER_500_1000;
    return GlobalVars.IMG_NUMBER_1000_MORE;
  }

  public final static int categAttSize(int num) {
    if (num == 0)
      return num;
    if (num >= 1 && num <= 100)
      return GlobalVars.ATTACHMENT_SIZE_1_100;
    if (num > 100 && num <= 500)
      return GlobalVars.ATTACHMENT_SIZE_100_500;
    if (num > 500 && num <= 1000)
      return GlobalVars.ATTACHMENT_SIZE_500_1000;
    return GlobalVars.ATTACHMENT_SIZE_1000_MORE;
  }

  public final static int categNumNonAsciiCharInSubj(int num) {
    if (num < 2)
      return num;
    if (num > 2 && num <= 5)
      return GlobalVars.NO_ASCII_CHAR_IN_SUBJECT_2_5;
    if (num > 5 && num <= 10)
      return GlobalVars.NO_ASCII_CHAR_IN_SUBJECT_5_10;
    return GlobalVars.NO_ASCII_CHAR_IN_SUBJECT_10_MORE;
  }

  public final static int categCountRecipients(int num) {
    if (num < 2)
      return num;
    return GlobalVars.RECIPIENTS_NUMBER_1_MORE;
  }

}
