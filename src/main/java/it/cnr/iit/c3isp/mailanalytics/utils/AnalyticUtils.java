package it.cnr.iit.c3isp.mailanalytics.utils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Component;

import com.google.common.base.Charsets;
import com.google.gson.Gson;

import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.basic.Element;
import iit.cnr.it.classificationengine.basic.TYPE;
import iit.cnr.it.classificationengine.basic.Utility;
import iit.cnr.it.classificationengine.basic.classifiers.RandomTreeForest;
import iit.cnr.it.classificationengine.clusterer.cctree.CCTree;
import iit.cnr.it.classificationengine.clusterer.cctree.Node;
import it.cnr.iit.c3isp.mailanalytics.c3isp.types.DataLakeBuffer;
import it.cnr.iit.c3isp.mailanalytics.c3isp.types.DpoInfo;
import it.cnr.iit.c3isp.mailanalytics.controllers.SpamClustererParameters;
import it.cnr.iit.c3isp.mailanalytics.json.ClassForEmailID;
import it.cnr.iit.c3isp.mailanalytics.json.ClassificationResult;
import it.cnr.iit.c3isp.mailanalytics.parser.Mail;
import it.cnr.iit.c3isp.mailanalytics.parser.Parser;

@Component
public class AnalyticUtils {

	private final static Logger log = Logger.getLogger(AnalyticUtils.class.getName());
	private RandomTreeForest<Integer> randomTreeForest;
	private DataSet<Integer> dataset;

	public AnalyticUtils() {
	}

	public void init() {
		dataset = retrieveData();
		randomTreeForest = new RandomTreeForest<>(dataset, Integer.class);
		randomTreeForest.train();
	}

	public DataSet<Integer> retrieveData() {

		ArrayList<String> featureLines = Utility.read("trainingSet.txt");

		DataSet<Integer> dataset = DataSet.buildDatasetClassifier(featureLines, "\n", ",", Integer.class);
		dataset.removeAttribute(0);
		return dataset;
	}

	public int[][] getConfusionMatrix() {
		return randomTreeForest.confusionMatrix(dataset);
	}

	private ArrayList<Element<Integer>> extractFeatures(DpoInfo dpoInfo, int id,
			HashMap<String, HashMap<String, String>> mailAttributes) {
		ArrayList<Element<Integer>> multipleElements = new ArrayList<Element<Integer>>();

		try {
			MultipleFiles[] dpofiles = readMultipleFile(dpoInfo.getFile().substring(7));
			for (MultipleFiles email : dpofiles) {
				HashMap<String, String> attributes = new HashMap<>();
				Mail mail = new Mail(id);
				// Parser parser = new Parser(new FileInputStream(new File(new
				// URI(dpoInfo.getFile()))), mail);
				// Decode data on other side, by processing encoded data
				byte[] emailDecoded = Base64.getDecoder().decode(email.getContent());
				String contentEmail = new String(emailDecoded);
				Parser parser = new Parser(contentEmail, mail);

				parser.parse();
				String features = mail.toString();
				Element<Integer> element = Element.buildClustererElement(features, ",", Integer.class);
				attributes.put("filename", email.getFilename());
				attributes.put("dpo", dpoInfo.getFile().substring(7));

				if (mail.getSender() != null && mail.getSender().length() > 0)
					attributes.put("sender", mail.getSender());

				if (mail.getRecipient() != null && mail.getRecipient().length() > 0)
					attributes.put("recipient", mail.getRecipient());

				if (mail.getSubject() != null && mail.getSubject().length() > 0)
					attributes.put("subject", mail.getSubject());

				mailAttributes.put(Integer.toString(id), attributes);
				id += 1;
				multipleElements.add(element);

			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return multipleElements;
	}

	private DataSet<Integer> buildDatasetFromEmails(DataLakeBuffer datalake, boolean features,
			HashMap<String, String> mailIdInDataset, HashMap<String, HashMap<String, String>> mailAttributes) {
		DataSet<Integer> toClassify = new DataSet<Integer>(Integer.class) {
		};

		toClassify.setQualification(TYPE.CLUSTERING);
		int id = 0;

		for (DpoInfo dpoInfo : datalake.getData()) {
			if (!features) {
				ArrayList<Element<Integer>> multipleElements = extractFeatures(dpoInfo, id, mailAttributes);

				for (Element<Integer> element : multipleElements) {
					id += 1;
					if (element != null) {
						toClassify.add(element);
					}
				}

			} else {
				String content = readFile(dpoInfo.getFile().substring(7));

				// String content = ISI_API.readDpo(dpoInfo.getDPO_id(), restTemplate);
				ArrayList<String> fileContent = new ArrayList<>();
				fileContent.addAll(Arrays.asList(content.split("\n")));
				DataSet<Integer> tmpToClassify = DataSet.buildDatasetClassifier(fileContent, "\n", ",", Integer.class);
				toClassify.setQualification(TYPE.CLASSIFIER);
				toClassify.append(tmpToClassify);
			}
		}

		return toClassify;
	}

	public DataSet<Integer> buildDatasetFromEmailsAndFillTable(DataLakeBuffer datalake,
			HashMap<String, String> mailIdInDataset, HashMap<String, HashMap<String, String>> mailAttributes,
			boolean featuresOnly) {
		DataSet<Integer> dataset = buildDatasetFromEmails(datalake, featuresOnly, mailIdInDataset, mailAttributes);

		if (featuresOnly) {
			for (int i = 0; i < dataset.getNumberOfElements(); i++) {
				mailIdInDataset.put("" + dataset.getElement(i).getFeature(0), datalake.getData()[0].getDPO_id());
			}
		}
		log.info("FEATURES:" + dataset.toString(true));
		return dataset;
	}

	public ClassificationResult classify(DataSet<Integer> testdataset, HashMap<String, String> mailIdInDataSet,
			HashMap<String, HashMap<String, String>> mailAttributes) {
		ClassificationResult classificationResult = new ClassificationResult();

		for (int i = 0; i < testdataset.getNumberOfElements(); i++) {
			Element<Integer> element = testdataset.getElement(i);
			System.out.println("dpoID " + mailAttributes.get(Integer.toString(i)).get("dpo"));
			System.out.println("filename " + mailAttributes.get(Integer.toString(i)).get("filename"));
			System.out.println("recipient " + mailAttributes.get(Integer.toString(i)).get("recipient"));
			System.out.println("subject " + mailAttributes.get(Integer.toString(i)).get("subject"));
			System.out.println("sender " + mailAttributes.get(Integer.toString(i)).get("sender"));

			ClassForEmailID classForEmailID = new ClassForEmailID();
			element.removeFeature(0);
			int classIndex = randomTreeForest.classify(element);
			String className = dataset.getLabelDistinct().get(classIndex);
			URI uri = null;
			try {
				uri = new URI(mailAttributes.get(Integer.toString(i)).get("dpo"));
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String[] segments = uri.getPath().split("/");
			String idStr = segments[segments.length - 1].replace(".dpo", "");

			classForEmailID.setMailId(idStr);
			classForEmailID.setDestination(mailAttributes.get(Integer.toString(i)).get("recipient"));
			classForEmailID.setSubject(mailAttributes.get(Integer.toString(i)).get("subject"));
			classForEmailID.setSource(mailAttributes.get(Integer.toString(i)).get("sender"));
			classForEmailID.setFilename(mailAttributes.get(Integer.toString(i)).get("filename"));

			classForEmailID.setClassifiedAs(className);
			log.info("Classification of email: " + element.toString() + " returned: " + className);
			classificationResult.addClassifiedEmail(classForEmailID);
		}

		return classificationResult;
	}

	public CCTree buildCCTree(SpamClustererParameters params, DataSet<Integer> dataset) {
		System.out.println("INFO: purity = " + params.getPurity() + ", minElementsThreshold = "
				+ params.getMinElementsThreshold());
		CCTree cctree = new CCTree(dataset);
		cctree.setPurityThreshold(params.getPurity());
		cctree.setMinElements(params.getMinElementsThreshold());
		return cctree;
	}

	public ClassificationResult clusterer(CCTree cctree, HashMap<String, HashMap<String, String>> mailAttributes,
			DataSet<Integer> testdataset) throws URISyntaxException {

		cctree.train();
		ClassificationResult classificationResult = new ClassificationResult();

		for (int i = 0; i < testdataset.getNumberOfElements(); i++) {
			ClassForEmailID classForEmailID = new ClassForEmailID();

			URI uri = new URI(mailAttributes.get(Integer.toString(i)).get("dpo"));
			String[] segments = uri.getPath().split("/");
			String idStr = segments[segments.length - 1];

			classForEmailID.setMailId(idStr.split(".dpo")[0]);
			classForEmailID.setDestination(mailAttributes.get(Integer.toString(i)).get("recipient"));
			classForEmailID.setSubject(mailAttributes.get(Integer.toString(i)).get("subject"));
			classForEmailID.setSource(mailAttributes.get(Integer.toString(i)).get("sender"));
			classForEmailID.setFilename(mailAttributes.get(Integer.toString(i)).get("filename"));

			Element<Integer> element = testdataset.getElement(i);
			System.out.println("dpoID " + mailAttributes.get(Integer.toString(i)).get("dpo"));
			System.out.println("filename " + mailAttributes.get(Integer.toString(i)).get("filename"));
			System.out.println("recipient " + mailAttributes.get(Integer.toString(i)).get("recipient"));
			System.out.println("subject " + mailAttributes.get(Integer.toString(i)).get("subject"));
			System.out.println("sender " + mailAttributes.get(Integer.toString(i)).get("sender"));
			// System.out.println("element " + element.toString());
			for (Node node : cctree.getClusters()) {
				String father = node.toString().split("Father: ")[1].split("leafId: ")[0];
				String leafId = node.toString().split("leafId: ")[1].split("Attribute")[0];
				String label = node.toString().split("Label: ")[1].split("------------")[0];
				String[] labels = label.trim().split("\n");
				for (String labelComputed : labels) {
					if (labelComputed.contains(element.toString())) {
						if (node.isLeaf()) {
							classForEmailID.setPurity(String.valueOf(node.getPurity()));
							classForEmailID.setLeafId(String.valueOf(node.getId()));
							classificationResult.addClassifiedEmail(classForEmailID);
							System.out.println("classForEmailID : " + new Gson().toJson(classForEmailID));
							break;
						}
					}
				}
			}
		}

		return classificationResult;
	}

	private String readFile(String path) {
		try {
			return FileUtils.readFileToString(new File(path), Charsets.UTF_8);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	private MultipleFiles[] readMultipleFile(String path) {
		try {
			String dpocontent = FileUtils.readFileToString(new File(path), Charsets.UTF_8);
			Gson gson = new Gson();
			log.severe("dpocontent in readMultipleFile: " + dpocontent);
			MultipleFiles[] dpoArray = gson.fromJson(dpocontent, MultipleFiles[].class);
			return dpoArray;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public String createFileFromString(String analytic, String content) throws Exception {
		long timestamp = (long) (System.currentTimeMillis() / 1e3);
		String randStr = RandomStringUtils.randomAlphanumeric(8);
		String path = Paths.get("/tmp/" + analytic + "_" + timestamp + "_" + randStr + ".log").toAbsolutePath()
				.toString();
		File file = File.createTempFile(path, "tmp");
		file.deleteOnExit();
		FileUtils.forceDeleteOnExit(file);
		FileUtils.writeByteArrayToFile(file, content.getBytes(StandardCharsets.UTF_8));
		return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
	}

}
