package it.cnr.iit.c3isp.mailanalytics.parser;

public class GlobalVars {
	//mail type
	public final static int PLAIN_TEXT = 1;	
	public final static int HTML_BASED = 2;
	public final static int IMAGE_BASED = 3;
	public final static int LINKS_ONLY = 4;
	//languages
	public final static int UNKNOWN_LANG = 0;
	public final static int ENGLISH_LANG = 1;
	public final static int ITALIAN_LANG = 2;
	public final static int FRENCH_LANG = 3;
	public final static int GERMAN_LANG = 4;
	public final static int SPANISH_LANG = 5;
	public final static int CHINESE_LANG = 6;
	public final static int ARAB_LANG = 7;
	public final static int PERSIAN_LANG = 8;
	public final static int JAPAN_LANG = 9;
	public final static int CROATIAN_LANG = 10;
	
	//attachment type
	public final static int NONE = 0;
	public final static int PDF = 1;
	public final static int EXEC = 2;
	public final static int DOC = 3;
	public final static int PIC = 4;
	public final static int TXT = 5;
	public final static int ZIP = 6;
	public final static int OTHER = 7;
	//spam classes
	
	//utils
	public final static String tempFile = "tempfile.txt";
	public final static String IPADDRESS_PATTERN = 
	        "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
	public final static String ASCII_PATTERN = "[^\\x00-\\x7F]";
	
	//from numerical to categorical values
	public final static int LENGTH_CLASS_0_1000 = 0;
	public final static int LENGTH_CLASS_1000_5000 = 1;
	public final static int LENGTH_CLASS_5000_10000 = 2;
	public final static int LENGTH_CLASS_10000_20000 = 3;
	public final static int LENGTH_CLASS_20000_30000 = 4;
	public final static int LENGTH_CLASS_30000_40000 = 5;
	public final static int LENGTH_CLASS_40000_50000 = 6;
	public final static int LENGTH_CLASS_50000_60000 = 7;
	public final static int LENGTH_CLASS_60000_70000 = 8;
	public final static int LENGTH_CLASS_70000_80000 = 9;
	public final static int LENGTH_CLASS_80000_90000 = 10;
	public final static int LENGTH_CLASS_90000_100000 = 11;
	public final static int LENGTH_CLASS_100000_MORE = 12;
	
	public final static int IP_CLASS_0=0;
	public final static int IP_CLASS_1_MORE=1;
		
	public final static int MISMATCHING_CLASS_0 = 0;
	public final static int MISMATCHING_CLASS_1 = 1;
	public final static int MISMATCHING_CLASS_2 = 2;
	public final static int MISMATCHING_CLASS_3_MORE = 3;
	
	public final static int MISMATCHING_VALUE_1 = 1;
	public final static int MISMATCHING_VALUE_2 = 2;
	
	public final static int LINKS_NUMBER_CLASS_0 = 0;
	public final static int LINKS_NUMBER_CLASS_1_5 = 1;
	public final static int LINKS_NUMBER_CLASS_6_10 = 2;
	public final static int LINKS_NUMBER_CLASS_10_100 = 3;
	public final static int LINKS_NUMBER_CLASS_100_MORE = 4;
	
	public final static int NUMBER_OF_DOMAIN_IN_LINKS_0 = 0;
	public final static int NUMBER_OF_DOMAIN_IN_LINKS_1 = 1;
	public final static int NUMBER_OF_DOMAIN_IN_LINKS_2 = 2;
	public final static int NUMBER_OF_DOMAIN_IN_LINKS_3 = 3;
	public final static int NUMBER_OF_DOMAIN_IN_LINKS_4 = 4;
	public final static int NUMBER_OF_DOMAIN_IN_LINKS_5 = 5;
	public final static int NUMBER_OF_DOMAIN_IN_LINKS_6_10 = 6;
	public final static int NUMBER_OF_DOMAIN_IN_LINKS_10_MORE = 7;
	
	public final static int AVG_DOTS_PER_LINKS_0 = 0;
	public final static int AVG_DOTS_PER_LINKS_1 = 1;
	public final static int AVG_DOTS_PER_LINKS_2 = 2;
	public final static int AVG_DOTS_PER_LINKS_3 = 3;
	public final static int AVG_DOTS_PER_LINKS_3_MORE = 4;
	
	public final static int NUMBER_OF_LINKS_WITH_AT_0 = 0;
	public final static int NUMBER_OF_LINKS_WITH_AT_1_MORE = 1;
	
	public final static int NUMBER_OF_LINKS_WITH_HEX_0 = 0;
	public final static int NUMBER_OF_LINKS_WITH_HEX_1 = 1;
	public final static int NUMBER_OF_LINKS_WITH_HEX_2 = 2;
	public final static int NUMBER_OF_LINKS_WITH_HEX_3 = 3;
	public final static int NUMBER_OF_LINKS_WITH_HEX_4 = 4;
	public final static int NUMBER_OF_LINKS_WITH_HEX_5 = 5;
	public final static int NUMBER_OF_LINKS_WITH_HEX_6_10 = 6;
	public final static int NUMBER_OF_LINKS_WITH_HEX_10_MORE = 7;
	
	public final static int NUMBER_OF_LINKS_WITH_NON_ASCII_0 = 0;
	public final static int NUMBER_OF_LINKS_WITH_NON_ASCII_1_MORE = 1;
	
	public final static int NUMBER_OF_ATTACHMENTS_0 = 0;
	public final static int NUMBER_OF_ATTACHMENTS_1 = 1;
	public final static int NUMBER_OF_ATTACHMENTS_2 = 2;
	public final static int NUMBER_OF_ATTACHMENTS_3 = 3;
	public final static int NUMBER_OF_ATTACHMENTS_4_MORE = 4;
	
	public final static int NUMBER_OF_WORDS_IN_SUBJECT_0 = 0;
	public final static int NUMBER_OF_WORDS_IN_SUBJECT_1_5 = 1;
	public final static int NUMBER_OF_WORDS_IN_SUBJECT_6_10 = 2;
	public final static int NUMBER_OF_WORDS_IN_SUBJECT_10_MORE = 3;
	
	public final static int NUMBER_OF_CHARS_IN_SUBJECT_0 = 0;
	public final static int NUMBER_OF_CHARS_IN_SUBJECT_1_10 = 1;
	public final static int NUMBER_OF_CHARS_IN_SUBJECT_10_20 = 2;
	public final static int NUMBER_OF_CHARS_IN_SUBJECT_20_MORE = 3;
	
	public final static int IMG_NUMBER_0 = 0;
	public final static int IMG_NUMBER_1_10 = 1;
	public final static int IMG_NUMBER_10_20 = 2;
	public final static int IMG_NUMBER_20_30 = 3;
	public final static int IMG_NUMBER_30_40 = 4;
	public final static int IMG_NUMBER_40_50 = 5;
	public final static int IMG_NUMBER_50_100 = 6;
	public final static int IMG_NUMBER_100_500 = 7;
	public final static int IMG_NUMBER_500_1000 = 8;
	public final static int IMG_NUMBER_1000_MORE = 9;
	
	
	public final static int ATTACHMENT_SIZE_0 = 0;
	public final static int ATTACHMENT_SIZE_1_100 = 1;
	public final static int ATTACHMENT_SIZE_100_500 = 2;
	public final static int ATTACHMENT_SIZE_500_1000 = 3;
	public final static int ATTACHMENT_SIZE_1000_MORE = 1;
	
	public final static int NO_ASCII_CHAR_IN_SUBJECT_0 = 0;
	public final static int NO_ASCII_CHAR_IN_SUBJECT_1 = 1;
	public final static int NO_ASCII_CHAR_IN_SUBJECT_2_5 = 2;
	public final static int NO_ASCII_CHAR_IN_SUBJECT_5_10 = 3;
	public final static int NO_ASCII_CHAR_IN_SUBJECT_10_MORE = 4;
	
	public final static int RECIPIENTS_NUMBER_0 = 0;
	public final static int RECIPIENTS_NUMBER_1 = 1;
	public final static int RECIPIENTS_NUMBER_1_MORE = 2;
}
