package it.cnr.iit.c3isp.mailanalytics.json.chart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import it.cnr.iit.c3isp.mailanalytics.json.ClassForEmailID;
import it.cnr.iit.c3isp.mailanalytics.json.ClassificationResult;

public class PieChart {
  @JsonProperty("key")
  String key = "";
  @JsonProperty("y")
  double y = 0.0;

  public PieChart() {

  }

  public void setKey(String key) {
    this.key = key;
  }

  public void setY(double y) {
    this.y = y;
  }

  public static ArrayList<PieChart> convertClassificationResultToPieChart(
      ClassificationResult classificationResult) {
    HashMap<String, Integer> emailPerLabel = new HashMap<>();
    double total = 0.0;
    for (ClassForEmailID classifiedEmail : classificationResult.getList()) {
      if (!emailPerLabel.containsKey(classifiedEmail.getClassifiedAs())) {
        emailPerLabel.put(classifiedEmail.getClassifiedAs(), 0);
      }
      emailPerLabel.put(classifiedEmail.getClassifiedAs(), emailPerLabel.get(
          classifiedEmail.getClassifiedAs()) + 1);
      total += 1;
    }
    ArrayList<PieChart> pieChart = new ArrayList<>();
    for (Map.Entry<String, Integer> entry : emailPerLabel.entrySet()) {
      PieChart chart = new PieChart();
      chart.setKey(entry.getKey());
      chart.setY(entry.getValue() / total);
      pieChart.add(chart);
    }

    return pieChart;
  }

}
