package it.cnr.iit.c3isp.mailanalytics.controllers;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import iit.cnr.it.classificationengine.basic.DataSet;
import iit.cnr.it.classificationengine.clusterer.cctree.CCTree;
import io.swagger.annotations.ApiModel;
import it.cnr.iit.c3isp.mailanalytics.async.AnalyticExecutor;
import it.cnr.iit.c3isp.mailanalytics.c3isp.types.DataLakeBuffer;
import it.cnr.iit.c3isp.mailanalytics.c3isp.types.DpoInfo;
import it.cnr.iit.c3isp.mailanalytics.c3isp.types.Paths;
import it.cnr.iit.c3isp.mailanalytics.json.ClassificationResult;
import it.cnr.iit.c3isp.mailanalytics.utils.AnalyticUtils;
import it.cnr.iit.common.analytic.AnalyticManager;
import it.cnr.iit.common.analytic.AnalyticResult;

@ApiModel(value = "MailAnalytics", description = "Analytics for spam detection and classification")
@RestController
@RequestMapping("/v1")
public class MailAnalyticsController {

	@Autowired
	private AnalyticUtils analyticUtils;

	@Autowired
	private AnalyticExecutor analyticExecutor;

	private AnalyticManager<String> analyticManager = new AnalyticManager<String>();

	@Value("${iai.api.spamDetect}")
	private String spamDetectUrl;

	private final static Logger log = Logger.getLogger(MailAnalyticsController.class.getName());

	public MailAnalyticsController() {
	}

	@RequestMapping(method = RequestMethod.POST, value = "/stopAnalytic/{sessionId}")
	public String stopAnalytic(@PathVariable String sessionId) {
		log.info("Stopping analytic with sessionId =" + sessionId);
		return analyticManager.stopAnalytic(sessionId);
	}

	@PostMapping(value = "/spamEmailClassify/{sessionId}")
	public AnalyticResult spamEmailClassify(@RequestBody SpamClassifyParameters params,
			@PathVariable String sessionId) {
		try {
			analyticUtils.init();
			HashMap<String, String> mailIdInDataset = new HashMap<>();
			HashMap<String, HashMap<String, String>> mailAttributes = new HashMap<>();
			DataSet<Integer> testdataset = analyticUtils.buildDatasetFromEmailsAndFillTable(params.getDataLakeBuffer(),
					mailIdInDataset, mailAttributes, params.isFeaturesOnly());

			Future<String> future = analyticExecutor.executeWithResult(analyticExecutor.CLASSIFY_ANALYTIC,
					analyticUtils, mailIdInDataset, mailAttributes, testdataset);
			analyticManager.getFutureMap().putFuture(sessionId, future);
			String result = future.get();
			AnalyticResult analyticResult = new AnalyticResult();
			analyticResult.setStatus("ok");
			analyticResult.addToResultArray(result.getBytes());
			analyticManager.getFutureMap().removeFuture(sessionId);
			return analyticResult;

		} catch (Exception e) {
			e.printStackTrace();
			return new AnalyticResult("error", e.getMessage());
		}

	}

	@PostMapping(value = "/spamEmailClusterer/{sessionId}")
	public AnalyticResult spamEmailClusterer(@RequestBody SpamClustererParameters params,
			@PathVariable String sessionId) {

		try {
			HashMap<String, String> mailIdInDataset = new HashMap<>();
			HashMap<String, HashMap<String, String>> mailAttributes = new HashMap<>();

			DataSet<Integer> testdataset = analyticUtils.buildDatasetFromEmailsAndFillTable(params.getDataLakeBuffer(),
					mailIdInDataset, mailAttributes, false);

			testdataset.removeAttribute(0);
			CCTree cctree = analyticUtils.buildCCTree(params, testdataset);
			ClassificationResult classificationResult = analyticUtils.clusterer(cctree, mailAttributes, testdataset);

			String result = classificationResult.toString();

			byte[] resultArray = result.getBytes();
			AnalyticResult analyticResult = new AnalyticResult();
			analyticResult.addToResultArray(resultArray);
			analyticResult.setStatus("ok");
			return analyticResult;

		} catch (URISyntaxException e) {
			e.printStackTrace();
			return new AnalyticResult("error", e.getMessage());
		}

	}

	@PostMapping(value = "/spamEmailDetect/{sessionId}")
	public AnalyticResult spamEmailDetect(@RequestBody DataLakeBuffer datalake, @PathVariable String sessionId) {

		Paths paths = new Paths();
		for (DpoInfo dpoInfo : datalake.getData()) {
			paths.addPath(dpoInfo.getFile());
		}

//		URI searchUri = UriComponentsBuilder.fromUriString(spamDetectUrl).build().toUri();

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
		body.addAll("paths", paths.getPaths());
		HttpEntity<?> httpEntity = new HttpEntity<Object>(body, requestHeaders);
		ResponseEntity<String> response = restTemplate.exchange(spamDetectUrl, HttpMethod.POST, httpEntity,
				String.class);

		log.info("SpamDetect result: " + response.getBody());

		ClassificationResult classificationResult = ClassificationResult.createFromSpamDetect(response.getBody(),
				datalake);

		String result = classificationResult.toString();

		byte[] resultArray = result.getBytes();
		AnalyticResult analyticResult = new AnalyticResult();
		analyticResult.addToResultArray(resultArray);
		analyticResult.setStatus("ok");
		return analyticResult;
	}

}
