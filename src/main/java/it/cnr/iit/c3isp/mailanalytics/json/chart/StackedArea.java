package it.cnr.iit.c3isp.mailanalytics.json.chart;

import java.util.ArrayList;
import java.util.List;

public class StackedArea {

  String key;
  List<List<Long>> values = new ArrayList<List<Long>>();

  public StackedArea() {

  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getKey() {
    return this.key;
  }

  public void setValues(List<Long> values) {
    this.values.add(values);
  }

  public void setValueOnTime(Long timestamp, Long value) {
    for (int i = 0; i < this.values.size(); i++) {
      System.out.println(timestamp + " " + this.values.get(i).get(0));
      if (this.values.get(i).get(0).equals(timestamp))
        this.values.get(i).set(1, value);
    }
  }

  public List<List<Long>> getValues() {
    return this.values;
  }

}
