package it.cnr.iit.c3isp.mailanalytics.parser;

import java.util.ArrayList;

public class MailClassified extends Mail {

  private boolean isSpam;
  private String spamClass;

  public MailClassified(int id) {
    super(id);
    // TODO Auto-generated constructor stub
  }

  public MailClassified(Mail mail) {
    super(mail);
    anonymizeFields();
  }

  public boolean isSpam() {
    return isSpam;
  }

  public void setSpam(boolean isSpam) {
    this.isSpam = isSpam;
  }

  public String getSpamClass() {
    return spamClass;
  }

  public void setSpamClass(String spamClass) {
    isSpam = true;
    this.spamClass = spamClass;
  }

  private void anonymizeFields() {
    texts = new ArrayList<>();
    // recipient = "";
    // sender = "";
    mailText = "";
  }

}
