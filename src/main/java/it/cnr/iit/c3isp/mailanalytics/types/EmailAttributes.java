
package it.cnr.iit.c3isp.mailanalytics.types;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "subject",
    "recipient_data",
    "sender",
    "attachment",
    "email_size",
    "email_format",
    "email_id",
    "body",
    "created",
    "email_language"
})
public class EmailAttributes {

    @JsonProperty("subject")
    private Subject subject;
    @JsonProperty("recipient_data")
    private RecipientData recipientData;
    @JsonProperty("sender")
    private Sender sender;
    @JsonProperty("attachment")
    private Attachment attachment;
    @JsonProperty("email_size")
    private Integer emailSize;
    @JsonProperty("email_format")
    private String emailFormat;
    @JsonProperty("email_id")
    private String emailId;
    @JsonProperty("body")
    private String body;
    @JsonProperty("created")
    private String created;
    @JsonProperty("email_language")
    private String emailLanguage;
    @JsonIgnore
    private Map<String, java.lang.Object> additionalProperties = new HashMap<String, java.lang.Object>();

    @JsonProperty("subject")
    public Subject getSubject() {
        return subject;
    }

    @JsonProperty("subject")
    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @JsonProperty("recipient_data")
    public RecipientData getRecipientData() {
        return recipientData;
    }

    @JsonProperty("recipient_data")
    public void setRecipientData(RecipientData recipientData) {
        this.recipientData = recipientData;
    }

    @JsonProperty("sender")
    public Sender getSender() {
        return sender;
    }

    @JsonProperty("sender")
    public void setSender(Sender sender) {
        this.sender = sender;
    }

    @JsonProperty("attachment")
    public Attachment getAttachment() {
        return attachment;
    }

    @JsonProperty("attachment")
    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @JsonProperty("email_size")
    public Integer getEmailSize() {
        return emailSize;
    }

    @JsonProperty("email_size")
    public void setEmailSize(Integer emailSize) {
        this.emailSize = emailSize;
    }

    @JsonProperty("email_format")
    public String getEmailFormat() {
        return emailFormat;
    }

    @JsonProperty("email_format")
    public void setEmailFormat(String emailFormat) {
        this.emailFormat = emailFormat;
    }

    @JsonProperty("email_id")
    public String getEmailId() {
        return emailId;
    }

    @JsonProperty("email_id")
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @JsonProperty("body")
    public String getBody() {
        return body;
    }

    @JsonProperty("body")
    public void setBody(String body) {
        this.body = body;
    }

    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    @JsonProperty("email_language")
    public String getEmailLanguage() {
        return emailLanguage;
    }

    @JsonProperty("email_language")
    public void setEmailLanguage(String emailLanguage) {
        this.emailLanguage = emailLanguage;
    }

    @JsonAnyGetter
    public Map<String, java.lang.Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, java.lang.Object value) {
        this.additionalProperties.put(name, value);
    }

}
