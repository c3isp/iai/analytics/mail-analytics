package it.cnr.iit.c3isp.mailanalytics.json;

import java.util.HashMap;

import it.cnr.iit.c3isp.mailanalytics.parser.MailClassified;

/**
 * This class represents an arc between a source and a destination.
 * <p>
 * Informations stored in the arc are:
 * <ol>
 * <li>Source point</li>
 * <li>Destination point</li>
 * <li>Number of arcs (number of email from the same source to the same
 * destination)</li>
 * <li>Timestamp of the email</li>
 * <li>Metadata: map of metadata belonging to the email</li>
 * </ol>
 * </p>
 * 
 * @author antonio
 *
 */
final public class Arc {

  private Point source;
  private Point dest;
  private int counter = 1;
  private String timestamp;
  private String classification;
  private HashMap<String, String> metadata;
  private String id;

  public static final Arc arcFromMailClassified(MailClassified mailClassified) {
    try {
      Arc arc = new Arc();
      if (mailClassified.getSenderPosition() == null
          || mailClassified.getRecipientPosition() == null) {
        return null;
      }
      arc.setSource(mailClassified.getSenderPosition());
      arc.setDest(mailClassified.getRecipientPosition());
      arc.setClassification(mailClassified.getSpamClass());
      arc.setTimestamp(mailClassified.getReceivingDate());
      arc.computeId();
      return arc;
    } catch (Exception exception) {
      exception.printStackTrace();
      return null;
    }
  }

  public Point getSource() {
    return source;
  }

  public void setSource(Point source) {
    this.source = source;
  }

  public Point getDest() {
    return dest;
  }

  public void setDest(Point dest) {
    this.dest = dest;
  }

  public int getCounter() {
    return counter;
  }

  public void setCounter(int counter) {
    this.counter = counter;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public HashMap<String, String> getMetadata() {
    return metadata;
  }

  public void setMetadata(HashMap<String, String> metadata) {
    this.metadata = metadata;
  }

  /**
   * Sets the id of this arc, this is used in order to increment counter if
   * there are many arcs with the same source and destination and the same
   * classification
   * 
   * @return the id of the arc
   */
  public String getId() {
    if (id == null) {
      computeId();
    }
    return id;
  }

  /**
   * Sets the id of this arc, this is used in order to increment counter if
   * there are many arcs with the same source and destination and the same
   * classification
   * 
   * @return the id of the arc
   */
  public void computeId() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(source.toString() + "-");
    stringBuilder.append(dest.toString() + "-");
    stringBuilder.append(classification);
    id = stringBuilder.toString();
  }

  public void setId(String s) {
    this.id = s;
  }

  public String getClassification() {
    return classification;
  }

  public void setClassification(String classification) {
    this.classification = classification;
  }

  public void incrementCounter() {
    this.counter += 1;
  }

}
